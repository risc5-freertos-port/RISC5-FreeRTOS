/*-----------------------------------------------------------------------*/
/* Low level disk I/O module SKELETON for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */
#include "string.h"     /* used to copy data from/to aligned buffer */
#include "sdc.h"
#include "stdio.h"

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

static DSTATUS stat = STA_NOINIT;
static LBA_t sdc_capacity;

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive number to identify the drive */
)
{
	switch (pdrv) {
        case 0 : /* only one drive usable */
            return stat;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
    switch (pdrv) {
        case 0: /* only one disk usable */
            sdc_capacity = SDC_Capacity();
            stat &= ~STA_NOINIT;
            return stat;
    }
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive number to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	LBA_t sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
    unsigned int alignment_buffer[128]; /* Word-aligned buffer with size of one sector */
    int counter;
    if (pdrv != 0) {
        return RES_PARERR; /*only one disk available */
    }
    if (stat & STA_NOINIT) {
        return RES_NOTRDY; /* disk must be initialized */
    }
    if (sector + count > sdc_capacity) {
        return RES_PARERR; /* there must be enough space on the disk */
    }
    for (counter = 0; counter < count; counter++) {
        SDC_Read(sector + counter, alignment_buffer, 1); /* read from the SD Card into aligned buffer */
        memcpy(buff + counter *FF_MIN_SS, alignment_buffer, FF_MIN_SS); /* copy sector into given buffer */
    }
    return RES_OK;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive number to identify the drive */
	const BYTE *buff,	/* Data to be written */
	LBA_t sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
    unsigned int alignment_buffer[128]; /* Word-aligned buffer with size of one sector */
    int counter;
    if (pdrv != 0) {
        return RES_PARERR; /*only one disk available */
    }
    if (stat & STA_NOINIT) {
        return RES_NOTRDY; /* disk must be initialized */
    }
    if (sector + count > sdc_capacity) {
        return RES_PARERR; /* there must be enough space on the disk */
    }
    for (counter = 0; counter < count; counter++) {
        memcpy(alignment_buffer, buff + counter * FF_MIN_SS, FF_MIN_SS); /* copy sector in aligned array */
        SDC_Write(sector + counter, alignment_buffer, 1); /* write to the SD Card */
    }
    return RES_OK;
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
) {
    if (pdrv != 0) {
        return RES_PARERR; /* only one volume available */
    }
    if (stat & STA_NOINIT) {
        return RES_NOTRDY;
    }
    switch (cmd) {
        case CTRL_SYNC:
            return RES_OK; /* each write operation is done in the disk_write function. */
        case GET_SECTOR_COUNT:
            *((LBA_t *)buff) = sdc_capacity;
            return RES_OK;
        case GET_SECTOR_SIZE:
            *((LBA_t *)buff) = 512;
            return RES_OK;
        case GET_BLOCK_SIZE:
            return RES_ERROR;  /* block size unavailable */
        case CTRL_TRIM:
            return RES_OK;
    }
    return RES_PARERR;
}

