/*
 * main.c -- main program
 */


#include "types.h"
#include "stdarg.h"
#include "iolib.h"
#include "promlib.h"


#define OS_START	32	/* partition-relative sector number */

#define SECTOR_SIZE	512	/* bytes per sector */


/**************************************************************/


extern unsigned int partStart;	/* absolute sector number */
extern unsigned int partSize;	/* in sectors */

extern unsigned int osSize;	/* in sectors */


/**************************************************************/


int main(void) {
  unsigned int *loadAddr;
  unsigned int loadSector;
  unsigned int numSectors;
  int i;

  printf("OS loader executing...\n");
  printf("\n");
  printf("FreeRTOS loader\n");
  printf("\n");
  printf("Loading %d sectors, starting at sector 0x%X\n",
         osSize, partStart + OS_START);
  loadAddr = (unsigned int *) 0;
  loadSector = partStart + OS_START;
  numSectors = osSize;
  i = 0;
  while (numSectors > 0) {
    sdcardRead(loadSector, loadAddr);
    loadAddr += SECTOR_SIZE / sizeof(unsigned int);
    loadSector++;
    numSectors--;
    printf(".");
    if (++i == 32) {
      i = 0;
      printf("\n");
    }
  }
  printf("\n");
  printf("\n");
  printf("Starting FreeRTOS...\n");
  printf("\n");
  return 0;
}
