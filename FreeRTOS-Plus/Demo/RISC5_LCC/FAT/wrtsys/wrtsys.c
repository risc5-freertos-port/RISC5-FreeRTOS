/*
 * wrtsys.c -- write a bootable operating system to a partition
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>


#define SECTOR_SIZE	512		/* bytes per sector */

#define MBR_PTBL_OFF	0x1BE
#define TYPE_FAT32_LBA	0x0C

#define VBR_BPB_OFF	0x0B
#define VBR_CODE_OFF	0x5A

#define LOADER_START	9
#define MAX_LOADER_SIZE	(OS_START - LOADER_START)

#define OS_START	32


/**************************************************************/


typedef struct {
  unsigned char boot;			/* MSB is 'bootable' flag */
  unsigned char firstSectorCHS[3];	/* not used */
  unsigned char type;			/* type of partition */
  unsigned char lastSectorCHS[3];	/* not used */
  unsigned int start;			/* first sector LBA */
  unsigned int size;			/* number of sectors */
} PartEntry;


/**************************************************************/


unsigned char vbr[] = {
  #include "vbr.dump"
};


unsigned char loader[] = {
  #include "loader.dump"
};


/**************************************************************/


void error(char *fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  printf("Error: ");
  vprintf(fmt, ap);
  printf("\n");
  va_end(ap);
  exit(1);
}


/**************************************************************/


void copySector(FILE *disk, unsigned int partStart,
                unsigned int src, unsigned int dst) {
  unsigned char buf[SECTOR_SIZE];

  fseek(disk, (partStart + src) * (long) SECTOR_SIZE, SEEK_SET);
  if (fread(buf, 1, SECTOR_SIZE, disk) != SECTOR_SIZE) {
    error("cannot read partition-relative sector %d", src);
  }
  fseek(disk, (partStart + dst) * (long) SECTOR_SIZE, SEEK_SET);
  if (fwrite(buf, 1, SECTOR_SIZE, disk) != SECTOR_SIZE) {
    error("cannot write partition-relative sector %d", dst);
  }
}


int main(int argc, char *argv[]) {
  char *diskName;
  char *partNmbr;
  char *pathToOS;
  FILE *disk;
  unsigned char buf[SECTOR_SIZE];
  char *endp;
  int pn, pi;
  PartEntry *partTbl;
  unsigned int partStart;
  unsigned int partSize;
  unsigned int vbrBytes;
  unsigned int loaderBytes;
  unsigned int loaderSize;
  unsigned int byteIndex;
  unsigned int bytesLeft;
  FILE *os;
  unsigned int osBytes;
  unsigned int osSize;

  /* check command line arguments */
  if (argc != 4) {
    printf("Usage: %s <disk image> <partition number> <path to OS binary>\n",
           argv[0]);
    exit(1);
  }
  diskName = argv[1];
  partNmbr = argv[2];
  pathToOS = argv[3];
  /* read MBR and determine partition start and size */
  disk = fopen(diskName, "r+b");
  if (disk == NULL) {
    error("cannot open disk image '%s'", diskName);
  }
  if (fread(buf, 1, SECTOR_SIZE, disk) != SECTOR_SIZE) {
    error("cannot read MBR from disk image '%s'", diskName);
  }
  if (buf[0x1FE] != 0x55 || buf[0x1FF] != 0xAA) {
    error("MBR of disk '%s' has no boot signature", diskName);
  }
  pn = strtol(partNmbr, &endp, 0);
  if (*endp != '\0') {
    error("cannot read partition number");
  }
  if (pn < 1 || pn > 4) {
    error("illegal partition number %d", pn);
  }
  pi = pn - 1;
  partTbl = (PartEntry *) &buf[MBR_PTBL_OFF];
  if (partTbl[pi].type == 0) {
    error("partition %d does not contain a file system", pn);
  }
  if ((partTbl[pi].boot & 0x80) == 0) {
    error("partition %d is not bootable", pn);
  }
  if (partTbl[pi].type != TYPE_FAT32_LBA) {
    error("partition %d has wrong type 0x%02X, must be FAT32_LBA (0x%02X)",
          pn, partTbl[pi].type, TYPE_FAT32_LBA);
  }
  partStart = partTbl[pi].start;
  partSize = partTbl[pi].size;
  printf("Partition %d: start = sector 0x%X, size = 0x%X sectors\n",
         pn, partStart, partSize);
  /* read VBR */
  fseek(disk, partStart * SECTOR_SIZE, SEEK_SET);
  if (fread(buf, 1, SECTOR_SIZE, disk) != SECTOR_SIZE) {
    error("cannot read VBR from partition %d", pn);
  }
  if (buf[0x1FE] != 0x55 || buf[0x1FF] != 0xAA) {
    error("VBR of partition %d has no boot signature", pn);
  }
  vbrBytes = sizeof(vbr);
  if (vbrBytes != SECTOR_SIZE) {
    error("size of VBR code is not exactly %d bytes", SECTOR_SIZE);
  }
  if (vbr[0x1FE] != 0x55 || vbr[0x1FF] != 0xAA) {
    error("VBR code has no boot signature");
  }
  /* patch VBR */
  loaderBytes = sizeof(loader);
  loaderSize = (loaderBytes + SECTOR_SIZE - 1) / SECTOR_SIZE;
  if (loaderSize > MAX_LOADER_SIZE) {
    error("loader code too big");
  }
  * (unsigned int *) &vbr[4] = loaderSize;
  memcpy(&buf[0x00], &vbr[0x00], VBR_BPB_OFF);
  memcpy(&buf[VBR_CODE_OFF], &vbr[VBR_CODE_OFF], vbrBytes - VBR_CODE_OFF);
  /* write VBR */
  fseek(disk, partStart * SECTOR_SIZE, SEEK_SET);
  if (fwrite(buf, 1, SECTOR_SIZE, disk) != SECTOR_SIZE) {
    error("cannot write VBR to partition %d", pn);
  }
  printf("Partition %d: VBR written (1 sector @ sector 0x%X)\n",
         pn, partStart);
  /* write backup sectors */
  printf("Backup partition-relative sector 0 in sector 6\n");
  copySector(disk, partStart, 0, 6);
  printf("Backup partition-relative sector 1 in sector 7\n");
  copySector(disk, partStart, 1, 7);
  printf("Backup partition-relative sector 2 in sector 8\n");
  copySector(disk, partStart, 2, 8);
  /* patch OS loader */
  os = fopen(pathToOS, "rb");
  if (os == NULL) {
    error("cannot open OS binary '%s'", pathToOS);
  }
  fseek(os, 0, SEEK_END);
  osBytes = ftell(os);
  fseek(os, 0, SEEK_SET);
  osSize = (osBytes + SECTOR_SIZE - 1) / SECTOR_SIZE;
  * (unsigned int *) &loader[4] = osSize;
  /* write OS loader */
  fseek(disk, (partStart + LOADER_START) * SECTOR_SIZE, SEEK_SET);
  byteIndex = 0;
  bytesLeft = loaderBytes;
  while (bytesLeft >= SECTOR_SIZE) {
    if (fwrite(loader + byteIndex, 1, SECTOR_SIZE, disk) != SECTOR_SIZE) {
      error("cannot write loader to partition %d", pn);
    }
    byteIndex += SECTOR_SIZE;
    bytesLeft -= SECTOR_SIZE;
  }
  if (bytesLeft > 0) {
    memset(buf, 0, SECTOR_SIZE);
    memcpy(buf, loader + byteIndex, bytesLeft);
    if (fwrite(buf, 1, SECTOR_SIZE, disk) != SECTOR_SIZE) {
      error("cannot write loader to partition %d", pn);
    }
  }
  printf("Partition %d: loader written (%d sectors @ sector 0x%X)\n",
         pn, loaderSize, partStart + LOADER_START);
  /* write OS */
  fseek(disk, (partStart + OS_START) * SECTOR_SIZE, SEEK_SET);
  bytesLeft = osBytes;
  while (bytesLeft >= SECTOR_SIZE) {
    if (fread(buf, 1, SECTOR_SIZE, os) != SECTOR_SIZE) {
      error("cannot read OS from file '%s'", pathToOS);
    }
    if (fwrite(buf, 1, SECTOR_SIZE, disk) != SECTOR_SIZE) {
      error("cannot write OS to partition %d", pn);
    }
    bytesLeft -= SECTOR_SIZE;
  }
  if (bytesLeft > 0) {
    memset(buf, 0, SECTOR_SIZE);
    if (fread(buf, 1, bytesLeft, os) != bytesLeft) {
      error("cannot read OS from file '%s'", pathToOS);
    }
    if (fwrite(buf, 1, SECTOR_SIZE, disk) != SECTOR_SIZE) {
      error("cannot write OS to partition %d", pn);
    }
  }
  fclose(os);
  printf("Partition %d: OS written (%d sectors @ sector 0x%X)\n",
         pn, osSize, partStart + OS_START);
  fclose(disk);
  return 0;
}
