//
// vbr.s -- volume boot record (code part only)
//

	.SET	loaderStart,9		// disk location of OS loader
	.SET	loadAddr,0xB00000	// where to load the OS loader

	.SET	serialOut,0xFFE01C	// serial output
	.SET	readSector,0xFFE020	// read sector from SD card

	// get some addresses listed in the load map
	.GLOBAL	vbr
	.GLOBAL	loaderSize
	.GLOBAL	BPB_start
	.GLOBAL	BPB_end
	.GLOBAL	vbr1
	.GLOBAL	load
	.GLOBAL	liftoff
	.GLOBAL	msg
	.GLOBAL	partStart
	.GLOBAL	partSize

vbr:
	B	vbr1			// branch around the following word

loaderSize:
	.WORD	0			// filled in when the VBR and the
					// OS loader are combined

	.BYTE	0, 0, 0			// pad to end of field "OEM name"

BPB_start:
	.SPACE	0x5A - 0x0B		// BIOS parameter block
BPB_end:

	.ALIGN	4

	// the volume boot record loads the OS loader from a fixed
	// partition-relative disk position into high memory
vbr1:
	SUB	R14,R14,(4+2)*4		// create stack frame
	STW	R15,R14,8		// save return address
	STW	R8,R14,12		// save register variable
	STW	R9,R14,16		// another one
	STW	R10,R14,20		// and a third one
	MOV	R4,partStart		// save partition info
	STW	R1,R4,0
	MOV	R4,partSize
	STW	R2,R4,0
	MOV	R8,msg			// pointer to string
strloop:
	LDB	R1,R8,0			// get char
	BEQ	load			// null - finished, go loading
	C	serialOut		// output char
	ADD	R8,R8,1			// bump pointer
	B	strloop			// next char
load:
	MOV	R8,partStart		// first absolute sector to load
	LDW	R8,R8,0
	ADD	R8,R8,loaderStart
	MOV	R9,loadAddr		// gets loaded here
	MOV	R10,loaderSize		// sector count
	LDW	R10,R10,0
ldloop:
	MOV	R1,R8			// first argument: sector
	MOV	R2,R9			// second argument: address
	C	readSector		// load a single sector
	ADD	R9,R9,512		// bump load address
	ADD	R8,R8,1			// and sector number
	SUB	R10,R10,1		// decrement sector count
	BNE	ldloop			// not yet finished?
	LDW	R15,R14,8		// restore return address
	LDW	R8,R14,12		// restore register variable
	LDW	R9,R14,16		// another one
	LDW	R10,R14,20		// and a third one
	ADD	R14,R14,(4+2)*4		// release stack frame
	MOV	R1,partStart		// load partition info
	LDW	R1,R1,0
	MOV	R2,partSize
	LDW	R2,R2,0
liftoff:
	B	loadAddr		// jump to loaded program

	// say what is going on
msg:
	.BYTE	0x0D, 0x0A
	.BYTE	"VBR executing..."
	.BYTE	0x0D, 0x0A, 0

	.ALIGN	4

partStart:
	.WORD	0
partSize:
	.WORD	0

	.SPACE	184			// adjust for sizeof(vbr) = 512

	.BYTE	0x00, 0x00		// reserved
	.BYTE	0x55, 0xAA		// boot record signature
