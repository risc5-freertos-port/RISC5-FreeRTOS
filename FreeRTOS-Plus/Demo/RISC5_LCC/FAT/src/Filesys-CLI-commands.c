/*
 * Filesys-CLI-commands.c -- basic commands using the file system
 */


#include <stdio.h>
#include <string.h>

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"
#include "Filesys-CLI-commands.h"
#include "ff.h"


#define NL		"\r\n"
#define MAX_PARAM_SIZE	50


/**************************************************************/


static BaseType_t prvCATCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString);

static BaseType_t prvCPCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString);

static BaseType_t prvRMCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString);

static BaseType_t prvMVCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString);

static BaseType_t prvMKDIRCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString);

static BaseType_t prvRMDIRCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString);

static BaseType_t prvCDCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString);

static BaseType_t prvPWDCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString);

static BaseType_t prvLSCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString);


/**************************************************************/


static const CLI_Command_Definition_t xCAT = {
  "cat",
  "\r\ncat <path>\r\n"
  "  show contents of file <path>\r\n",
  prvCATCmd,
  1
};


static const CLI_Command_Definition_t xCP = {
  "cp",
  "\r\ncp <path1> <path2>\r\n"
  "  copy file <path1> to file <path2>\r\n",
  prvCPCmd,
  2
};


static const CLI_Command_Definition_t xRM = {
  "rm",
  "\r\nrm <path>\r\n"
  "  remove file <path>\r\n",
  prvRMCmd,
  1
};


static const CLI_Command_Definition_t xMV = {
  "mv",
  "\r\nmv <path1> <path2>\r\n"
  "  move file <path1> to file <path2>\r\n",
  prvMVCmd,
  2
};


static const CLI_Command_Definition_t xMKDIR = {
  "mkdir",
  "\r\nmkdir <path>\r\n"
  "  make directory <path>\r\n",
  prvMKDIRCmd,
  1
};


static const CLI_Command_Definition_t xRMDIR = {
  "rmdir",
  "\r\nrmdir <path>\r\n"
  "  remove empty directory <path>\r\n",
  prvRMDIRCmd,
  1
};


static const CLI_Command_Definition_t xCD = {
  "cd",
  "\r\ncd <path>\r\n"
  "  change current working directory to <path>\r\n",
  prvCDCmd,
  1
};


static const CLI_Command_Definition_t xPWD = {
  "pwd",
  "\r\npwd\r\n"
  "  print current working directory\r\n",
  prvPWDCmd,
  0
};


static const CLI_Command_Definition_t xLS = {
  "ls",
  "\r\nls <path>\r\n"
  "  show contents of directory <path>\r\n",
  prvLSCmd,
  1
};


/**************************************************************/


static FATFS filsys;


void vRegisterFilesysCLICommands(void) {
  FRESULT res;

  /* register all commands */
  FreeRTOS_CLIRegisterCommand(&xCAT);
  FreeRTOS_CLIRegisterCommand(&xCP);
  FreeRTOS_CLIRegisterCommand(&xRM);
  FreeRTOS_CLIRegisterCommand(&xMV);
  FreeRTOS_CLIRegisterCommand(&xMKDIR);
  FreeRTOS_CLIRegisterCommand(&xRMDIR);
  FreeRTOS_CLIRegisterCommand(&xCD);
  FreeRTOS_CLIRegisterCommand(&xPWD);
  FreeRTOS_CLIRegisterCommand(&xLS);
  /* mount the default partition */
  res = f_mount(&filsys, "", 1);
  printf("\r\n");
  if (res != FR_OK) {
    printf("Error: cannot mount the default file system, result = %d.\r\n",
           res);
  } else {
    printf("Success: default file system mounted.\r\n");
  }
}


/**************************************************************/


static BaseType_t prvCATCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString) {
  static BaseType_t firstCall = pdTRUE;
  static FIL fp;
  static char inbuf[200];
  static unsigned int numchars;		/* number of chars in inbuf[] */
  static unsigned int readindex;	/* index of next char to be read */
  const char *pcParam;
  BaseType_t xParamStrLen;
  char path[MAX_PARAM_SIZE];
  char c;

  if (firstCall) {
    pcParam = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParamStrLen);
    strncpy(path, pcParam, xParamStrLen);
    path[xParamStrLen] = '\0';
    if (f_open(&fp, path, FA_READ) != FR_OK) {
      sprintf(pcWriteBuffer,
              "error: cannot open file '%s'\r\n",
              path);
      return pdFALSE;
    }
    firstCall = pdFALSE;
    numchars = 0;
    readindex = 0;
  }
  while (1) {
    if (readindex == numchars) {
      f_read(&fp, inbuf, 200, &numchars);
      if (numchars == 0) {
        *pcWriteBuffer = '\0';
        f_close(&fp);
        firstCall = pdTRUE;
        return pdFALSE;
      }
      readindex = 0;
    }
    c = inbuf[readindex++];
    if (c == '\n') {
      *pcWriteBuffer++ = '\r';
      xWriteBufferLen--;
    }
    *pcWriteBuffer++ = c;
    xWriteBufferLen--;
    if (xWriteBufferLen < 3) {
      *pcWriteBuffer = '\0';
      return pdTRUE;
    }
  }
  /* never reached */
  return pdFALSE;
}


static BaseType_t prvCPCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString) {
  const char *pcParam1;
  BaseType_t xParamStrLen1;
  char path1[MAX_PARAM_SIZE];
  const char *pcParam2;
  BaseType_t xParamStrLen2;
  char path2[MAX_PARAM_SIZE];
  FIL fp1;
  FIL fp2;
  unsigned char buffer[200];
  unsigned int n;
  unsigned int m;

  pcParam1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParamStrLen1);
  strncpy(path1, pcParam1, xParamStrLen1);
  path1[xParamStrLen1] = '\0';
  pcParam2 = FreeRTOS_CLIGetParameter(pcCommandString, 2, &xParamStrLen2);
  strncpy(path2, pcParam2, xParamStrLen2);
  path2[xParamStrLen2] = '\0';
  /* open files */
  if (f_open(&fp1, path1, FA_READ) != FR_OK) {
    sprintf(pcWriteBuffer,
            "error: cannot open file '%s' for read\r\n",
            path1);
    return pdFALSE;
  }
  if (f_open(&fp2, path2, FA_CREATE_ALWAYS | FA_WRITE) != FR_OK) {
    sprintf(pcWriteBuffer,
            "error: cannot open file '%s' for write\r\n",
            path2);
    f_close(&fp1);
    return pdFALSE;
  }
  /* copy */
  while (1) {
    f_read(&fp1, buffer, 200, &n);
    if (n > 0) {
      f_write(&fp2, buffer, n, &m);
    } else {
      m = 0;
    }
    if (m < 200) {
      if (m < n) {
        sprintf(pcWriteBuffer,
                "cannot write file '%s'",
                path2);
      } else {
        *pcWriteBuffer = '\0';
      }
      f_close(&fp1);
      f_close(&fp2);
      return pdFALSE;
    }
  }
  /* never reached */
  return pdFALSE;
}


static BaseType_t prvRMCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString) {
  const char *pcParam;
  BaseType_t xParamStrLen;
  char path[MAX_PARAM_SIZE];
  FILINFO fno;

  pcParam = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParamStrLen);
  strncpy(path, pcParam, xParamStrLen);
  path[xParamStrLen] = '\0';
  if (f_stat(path, &fno) != FR_OK) {
    sprintf(pcWriteBuffer,
            "error: '%s' not found\r\n",
            path);
    return pdFALSE;
  }
  if ((fno.fattrib & AM_DIR) != 0) {
    sprintf(pcWriteBuffer,
            "error: '%s' is a directory\r\n",
            path);
    return pdFALSE;
  }
  if (f_unlink(path) != FR_OK) {
    sprintf(pcWriteBuffer,
            "error: cannot remove file '%s'\r\n",
            path);
  } else {
    *pcWriteBuffer = '\0';
  }
  return pdFALSE;
}


static BaseType_t prvMVCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString) {
  const char *pcParam1;
  BaseType_t xParamStrLen1;
  char path1[MAX_PARAM_SIZE];
  const char *pcParam2;
  BaseType_t xParamStrLen2;
  char path2[MAX_PARAM_SIZE];

  pcParam1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParamStrLen1);
  strncpy(path1, pcParam1, xParamStrLen1);
  path1[xParamStrLen1] = '\0';
  pcParam2 = FreeRTOS_CLIGetParameter(pcCommandString, 2, &xParamStrLen2);
  strncpy(path2, pcParam2, xParamStrLen2);
  path2[xParamStrLen2] = '\0';
  if (f_rename(path1, path2) != FR_OK) {
    sprintf(pcWriteBuffer,
            "error: cannot move '%s' to '%s'\r\n",
            path1, path2);
  } else {
    *pcWriteBuffer = '\0';
  }
  return pdFALSE;
}


static BaseType_t prvMKDIRCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString) {
  const char *pcParam;
  BaseType_t xParamStrLen;
  char path[MAX_PARAM_SIZE];

  pcParam = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParamStrLen);
  strncpy(path, pcParam, xParamStrLen);
  path[xParamStrLen] = '\0';
  if (f_mkdir(path) != FR_OK) {
    sprintf(pcWriteBuffer,
            "error: cannot create directory '%s'\r\n",
            path);
  } else {
    *pcWriteBuffer = '\0';
  }
  return pdFALSE;
}


static BaseType_t prvRMDIRCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString) {
  const char *pcParam;
  BaseType_t xParamStrLen;
  char path[MAX_PARAM_SIZE];
  FILINFO fno;

  pcParam = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParamStrLen);
  strncpy(path, pcParam, xParamStrLen);
  path[xParamStrLen] = '\0';
  if (f_stat(path, &fno) != FR_OK) {
    sprintf(pcWriteBuffer,
            "error: '%s' not found\r\n",
            path);
    return pdFALSE;
  }
  if ((fno.fattrib & AM_DIR) == 0) {
    sprintf(pcWriteBuffer,
            "error: '%s' is not a directory\r\n",
            path);
    return pdFALSE;
  }
  if (f_unlink(path) != FR_OK) {
    sprintf(pcWriteBuffer,
            "error: cannot remove directory '%s'\r\n",
            path);
  } else {
    *pcWriteBuffer = '\0';
  }
  return pdFALSE;
}


static BaseType_t prvCDCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString) {
  const char *pcParam;
  BaseType_t xParamStrLen;
  char path[MAX_PARAM_SIZE];

  pcParam = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParamStrLen);
  strncpy(path, pcParam, xParamStrLen);
  path[xParamStrLen] = '\0';
  if (f_chdir(path) != FR_OK) {
    sprintf(pcWriteBuffer,
            "error: cannot change the current working directory to '%s'\r\n",
            path);
  } else {
    *pcWriteBuffer = '\0';
  }
  return pdFALSE;
}


static BaseType_t prvPWDCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString) {
  if (f_getcwd(pcWriteBuffer, xWriteBufferLen) != FR_OK) {
    sprintf(pcWriteBuffer,
            "error: cannot print the current working directory\r\n");
  } else {
    strcat(pcWriteBuffer, NL);
  }
  return pdFALSE;
}


static char *months[] = {
  "Jan", "Feb", "Mar",
  "Apr", "May", "Jun",
  "Jul", "Aug", "Sep",
  "Oct", "Nov", "Dec"
};


static void formatLSoutput(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        FILINFO *fno) {
  unsigned short s;
  int year, month, day;
  int hour, minute;

  s = fno->fdate;
  year = ((s >> 9) & 0x7F);
  month = ((s >> 5) & 0x0F);
  day = (s >> 0) & 0x1F;
  s = fno->ftime;
  hour = (s >> 11) & 0x1F;
  minute = (s >> 5) & 0x3F;
  sprintf(pcWriteBuffer,
          "[%c] %6d %04d-%s-%02d %02d:%02d %s\r\n",
          (fno->fattrib & AM_DIR) ? 'd' : '-',
          fno->fsize,
          year + 1980, months[month - 1], day,
          hour, minute,
          fno->fname);
}


static BaseType_t prvLSCmd(
        char *pcWriteBuffer,
        size_t xWriteBufferLen,
        const char *pcCommandString) {
  static BaseType_t firstCall = pdTRUE;
  static DIR dir;
  const char *pcParam;
  BaseType_t xParamStrLen;
  char path[MAX_PARAM_SIZE];
  FILINFO fno;

  if (firstCall) {
    pcParam = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParamStrLen);
    strncpy(path, pcParam, xParamStrLen);
    path[xParamStrLen] = '\0';
    if (f_findfirst(&dir, &fno, path, "*") != FR_OK ||
        fno.fname[0] == '\0') {
      *pcWriteBuffer = '\0';
      return pdFALSE;
    }
    formatLSoutput(pcWriteBuffer, xWriteBufferLen, &fno);
    firstCall = pdFALSE;
    return pdTRUE;
  }
  if (f_findnext(&dir, &fno) != FR_OK ||
      fno.fname[0] == '\0') {
    f_closedir(&dir);
    *pcWriteBuffer = '\0';
    firstCall = pdTRUE;
    return pdFALSE;
  }
  formatLSoutput(pcWriteBuffer, xWriteBufferLen, &fno);
  return pdTRUE;
}
