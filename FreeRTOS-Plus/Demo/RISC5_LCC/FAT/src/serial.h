/*
 * serial.h -- interface to serial line device
 */


#ifndef _SERIAL_H_
#define _SERIAL_H_


typedef int xComPortHandle;


xComPortHandle xSerialPortInitMinimal(int baudRate, int queueLen);
BaseType_t xSerialGetChar(int port, signed char *pc, TickType_t delay);
void xSerialPutChar(int port, signed char c, TickType_t delay);
void vSerialPutString(int port, signed char *msg, short len);


#endif /* _SERIAL_H_ */
