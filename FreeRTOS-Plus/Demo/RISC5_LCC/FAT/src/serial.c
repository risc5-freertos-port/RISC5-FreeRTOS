/*
 * serial.c -- interface to serial line device
 */


#include "FreeRTOS.h"
#include "serial.h"
#include "RS232/rs232.h"


xComPortHandle xSerialPortInitMinimal(int baudRate, int queueLen) {
  static int currPort = 0;

  RS232_Init(RS232_BAUD_RATE_9600, RS232_0);
  return currPort++;
}


BaseType_t xSerialGetChar(int port, signed char *pc, TickType_t delay) {
  if (!RS232_RCV_Ready(RS232_0)) {
    return pdFAIL;
  }
  *pc = (signed char) RS232_Read_i(RS232_0);
  return pdPASS;
}


void xSerialPutChar(int port, signed char c, TickType_t delay) {
  while (!RS232_XMT_Ready(RS232_0)) ;
  RS232_Write_i(c, RS232_0);
}


void vSerialPutString(int port, signed char *msg, short len) {
  int i;

  for (i = 0; i < len; i++) {
    xSerialPutChar(port, *msg++, portMAX_DELAY);
  }
}
