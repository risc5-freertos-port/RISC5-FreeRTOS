/*
 * Filesys-CLI-commands.h -- basic commands using the file system
 */


#ifndef _FILESYS_CLI_COMMANDS_H_
#define _FILESYS_CLI_COMMANDS_H_


void vRegisterFilesysCLICommands(void);


#endif /* _FILESYS_CLI_COMMANDS_H_ */
