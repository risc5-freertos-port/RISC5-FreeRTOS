/*
 * UARTCommandConsole.h -- implement a command console over RS232
 */


#ifndef _UART_COMMAND_CONSOLE_H_
#define _UART_COMMAND_CONSOLE_H_


void vUARTCommandConsoleStart(uint16_t usStackSize, UBaseType_t uxPriority);


#endif /* _UART_COMMAND_CONSOLE_H_ */
