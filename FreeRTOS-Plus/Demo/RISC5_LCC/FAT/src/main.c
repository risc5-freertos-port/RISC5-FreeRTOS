/*
 * main.c -- main program
 */


#include "FreeRTOS.h"
#include "task.h"
#include "LEDs/leds.h"
#include "FreeRTOS_CLI.h"
#include "serial.h"
#include "UARTCommandConsole.h"
#include "Sample-CLI-commands.h"
#include "Filesys-CLI-commands.h"


#define CLI_STACK_SIZE		1024
#define CLI_PRIO		(tskIDLE_PRIORITY + 4)

#define BLINK_STACK_SIZE	256
#define BLINK_PRIO		(tskIDLE_PRIORITY + 4)


/**************************************************************/


/*
 * Create a task for the command line interpreter.
 */
static void createCliTask(void) {
  vRegisterSampleCLICommands();
  vRegisterFilesysCLICommands();
  vUARTCommandConsoleStart(CLI_STACK_SIZE, CLI_PRIO);
}


/**************************************************************/


static unsigned int led_state = 0;


static void led_toggle(unsigned int which) {
  taskENTER_CRITICAL();
  led_state ^= which;
  LEDs_Write(led_state);
  taskEXIT_CRITICAL();
}


/*
 * The first blink task.
 */
static void blink1Task(void *pvParameters) {
  while (1) {
    vTaskDelay(1000);
    led_toggle(0x10);
  }
}


void createBlink1Task(void) {
  xTaskCreate(blink1Task, "blink1", BLINK_STACK_SIZE,
              NULL, BLINK_PRIO, NULL);
}


/*
 * The second blink task.
 */
static void blink2Task(void *pvParameters) {
  vTaskDelay(500);
  while (1) {
    vTaskDelay(1000);
    led_toggle(0x20);
  }
}


void createBlink2Task(void) {
  xTaskCreate(blink2Task, "blink2", BLINK_STACK_SIZE,
              NULL, BLINK_PRIO, NULL);
}


/**************************************************************/


void main(void) {
  LEDs_Write(0x00);
  createCliTask();
  createBlink1Task();
  createBlink2Task();
  vTaskStartScheduler();
  /* should not be reached */
  LEDs_Write(LED_ALL);
}


/**************************************************************/


/*
 * If an assertion fails, this program gets
 * trapped in an infinte loop.
 */
void vAssertCalled(const char *fileName, int line) {

  taskENTER_CRITICAL();

  LEDs_Write(LED_ALL);
  while (1) ;

  taskEXIT_CRITICAL();
}


/**************************************************************/


void putchar(int c) {
  xSerialPutChar(0, (char) c, portMAX_DELAY);
}
