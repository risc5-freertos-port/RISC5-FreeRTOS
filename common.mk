# This must be the first line in this file:
ROOT_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

ifndef RISC5_BUILD
RISC5_BUILD := $(ROOT_DIR)../risc5-c-toolchain/build
$(warning Environment variable RISC5_BUILD pointing to toolchain build directory is not set, falling back to sibling directory $(RISC5_BUILD))
endif

_RISC5_BUILD := $(RISC5_BUILD)
RISC5_BUILD := $(realpath $(RISC5_BUILD))

ifeq ($(RISC5_BUILD),)
$(error RISC5 toolchain build directory not found ($(_RISC5_BUILD)))
endif
