/*
 * FreeRTOS Kernel <DEVELOPMENT BRANCH>
 * Copyright (C) 2021 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * SPDX-License-Identifier: MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * https://www.FreeRTOS.org
 * https://github.com/FreeRTOS
 *
 */

#ifndef PORTMACRO_H
#define PORTMACRO_H

#include <risc5.h>

/*-----------------------------------------------------------
 * Port specific definitions.
 *
 * The settings in this file configure FreeRTOS correctly for the
 * given hardware and compiler.
 *
 * These settings should not be altered.
 *-----------------------------------------------------------
 */

#ifndef configKERNEL_INTERRUPT_PRIORITY
	#error "Please define configKERNEL_INTERRUPT_PRIORITY"
#endif

#ifndef configMAX_API_CALL_INTERRUPT_PRIORITY
	#define configMAX_API_CALL_INTERRUPT_PRIORITY configKERNEL_INTERRUPT_PRIORITY
#endif

#if(configMAX_API_CALL_INTERRUPT_PRIORITY < configKERNEL_INTERRUPT_PRIORITY)
	#error "configMAX_API_CALL_INTERRUPT_PRIORITY must be greater than configKERNEL_INTERRUPT_PRIORITY"
#endif

/* Type definitions. */
#define portCHAR				char
#define portFLOAT				float
#define portDOUBLE				double
#define portLONG				long
#define portSHORT				short
#define portSTACK_TYPE			uint32_t
#define portBASE_TYPE			int
#define portPOINTER_SIZE_TYPE	uint32_t

typedef portSTACK_TYPE StackType_t;
typedef portBASE_TYPE BaseType_t;
typedef unsigned portBASE_TYPE UBaseType_t;

#if( configUSE_16_BIT_TICKS == 1 )
	typedef uint16_t TickType_t;
	#define portMAX_DELAY ( TickType_t ) 0xffff
#else
	typedef uint32_t TickType_t;
	#define portMAX_DELAY ( TickType_t ) 0xffffffffUL
#endif

/*-----------------------------------------------------------*/

/* Critical section management. */
#define portCRITICAL_NESTING_IN_TCB 1
extern void vTaskEnterCritical( void );
extern void vTaskExitCritical( void );
#define portENTER_CRITICAL()								vTaskEnterCritical()
#define portEXIT_CRITICAL()									vTaskExitCritical()

extern UBaseType_t xGetAndClearInterruptMask(void);
extern void vRestoreInterruptMask(UBaseType_t ulMask);
#define portSET_INTERRUPT_MASK_FROM_ISR()					xGetAndClearInterruptMask()
#define portCLEAR_INTERRUPT_MASK_FROM_ISR( x )				vRestoreInterruptMask( x )

extern void vDisableLowPriorityInterrupts(void);
extern void vEnableLowPriorityInterrupts(void);
#define portDISABLE_INTERRUPTS()							vDisableLowPriorityInterrupts()
#define portENABLE_INTERRUPTS()								vEnableLowPriorityInterrupts()
/*-----------------------------------------------------------*/

/* Architecture specifics. */
#define portSTACK_GROWTH	( -1 )
#define portTICK_PERIOD_MS	( ( TickType_t ) 1000 / configTICK_RATE_HZ )
#define portBYTE_ALIGNMENT	4

#define portNOP() _asm_NOP()
/*-----------------------------------------------------------*/

/* Kernel utilities. */
extern void vPortYield( void );
#define portYIELD() vPortYield()
#define portYIELD_FROM_ISR( x ) do { if( ( x ) != pdFALSE ) { portYIELD(); } } while( 0 )

extern void vPortYieldFromTick( void );

/* Task function macros as described on the FreeRTOS.org WEB site. */
#define portTASK_FUNCTION_PROTO( vFunction, pvParameters ) void vFunction( void *pvParameters )
#define portTASK_FUNCTION( vFunction, pvParameters ) void vFunction( void *pvParameters )

#endif /* PORTMACRO_H */


