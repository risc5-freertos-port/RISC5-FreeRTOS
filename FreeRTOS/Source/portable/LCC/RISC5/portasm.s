// Import global symbols
.GLOBAL vTaskSwitchContext
.GLOBAL xTaskIncrementTick
.GLOBAL pxCurrentTCB
.GLOBAL portDisableIRQMask
.GLOBAL risc5_ivt

// Export public symbols
.GLOBAL vDisableLowPriorityInterrupts
.GLOBAL vEnableLowPriorityInterrupts
.GLOBAL xGetAndClearInterruptMask
.GLOBAL vRestoreInterruptMask
.GLOBAL vPortYield
.GLOBAL vPortRestoreContext
.GLOBAL vPortYieldFromTick
.GLOBAL vIrqDispatcher
.GLOBAL contextFrameSize

// Constants
.SET contextFrameSize,	17*4
.SET enableIrqMask,		0xFFFF

//
// NOTE:
//
// The functions in this file should never be called directly from application code.
// Use the following portXXX macros defined in `portmacro.h` or the corresponding
// taskXXX macros where available:
//
// taskDISABLE_INTERRUPTS				(vDisableLowPriorityInterrupts)
// taskENABLE_INTERRUPTS				(vEnableLowPriorityInterrupts)
// portSET_INTERRUPT_MASK_FROM_ISR		(xGetAndClearInterruptMask)
// portCLEAR_INTERRUPT_MASK_FROM_ISR	(vRestoreInterruptMask)
// taskYIELD							(vPortYield)
// portYIELD_FROM_ISR					(vPortYield)
//
// Never call `vPortYieldFromTick` or `vIrqDispatcher` directly.
//

.CODE

///////////////////////////////////////////////////////////////////////////////////////
//
// void vDisableLowPriorityInterrupts(void)
//
///////////////////////////////////////////////////////////////////////////////////////

vDisableLowPriorityInterrupts:
	GETS R4, 3							// get PSW
	// load portDisableIRQMask
	MOV R5, portDisableIRQMask		
	LDW R5, R5, 0
	AND	R4, R4, R5						// disable low priority interrupts in mask
	PUTS R4, 3							// store PSW
	B R15

///////////////////////////////////////////////////////////////////////////////////////
//
// void vEnableLowPriorityInterrupts(void)
//
///////////////////////////////////////////////////////////////////////////////////////

vEnableLowPriorityInterrupts:
	GETS R4, 3
	IOR	R4, R4, enableIrqMask
	PUTS R4, 3
	B R15

///////////////////////////////////////////////////////////////////////////////////////
//
// UBaseType_t xGetAndClearInterruptMask(void)
//
///////////////////////////////////////////////////////////////////////////////////////

xGetAndClearInterruptMask:
	GETS R0, 3							// get PSW
	// load portDisableIRQMask
	MOV R4, portDisableIRQMask
	LDW R4, R4, 0
	AND	R4, R0, R4						// disable low priority interrupts in mask
	PUTS R4, 3							// store PSW
	AND R0, R0, 0xFFFF					// mask interrupt mask
	B R15

///////////////////////////////////////////////////////////////////////////////////////
//
// void vRestoreInterruptMask(UBaseType_t ulMask)
//
///////////////////////////////////////////////////////////////////////////////////////

vRestoreInterruptMask:
	GETS R4, 3							// get PSW
	IOR R4, R4, R1						// restore interrupt mask
	PUTS R4, 3							// store PSW
	B R15

///////////////////////////////////////////////////////////////////////////////////////
//
// void vPortYield(void)
//
// NOTE: This function has a second entrypoint `void vPortRestoreContext(void)`
//       used exclusively for bootstrapping the scheduler and which must never
//       be called from application code.
//
///////////////////////////////////////////////////////////////////////////////////////

vPortYield:
vPortSaveContext:
	GETS R0, 3							// temporarily save PSW in R0 (to save interrupt mask)

	SUB R14, R14, 1*4
	STW R15, R14, 0*4					// save R15 because of the call in next instruction
	C vDisableLowPriorityInterrupts
	LDW R15, R14, 0*4					// restore R15 
	ADD R14, R14, 1*4

	SUB	R14, R14, contextFrameSize		// calculate the new stack pointer for saving context
	STW R8, R14, 8*4					// save R8
	STW R9, R14, 9*4					// save R9
	STW R10, R14, 10*4					// save R10
	STW R11, R14, 11*4					// save R11
	STW R15, R14, 15*4					// save R15 at the stack position of X (for RTI)

	// set P-Flag and clear I-Flag to simulate hardware behaviour
	IOR R0, R0, 0x04000000
	AND R0, R0, 0xf7ffffff
	STW R0, R14, 16*4					// save prepared PSW on stack

	MOV R0, pxCurrentTCB				// get pointer to pxCurrentTCB
	LDW R0, R0, 0						// load pxCurrentTCB
	STW R14, R0, 0						// store stack pointer in pxCurrentTCB->pxTopOfStack

	C vTaskSwitchContext				// Call the scheduler.
vPortRestoreContext:
	MOV R0, pxCurrentTCB				// get pointer to pxCurrentTCB
	LDW R0, R0, 0						// load pxCurrentTCB
	LDW R14, R0, 0						// store pxCurrentTCB->pxTopOfStack in stack pointer

	LDW R2, R14, 15*4					// load X register from stack
	LDW R1, R14, 14*4					// load H register from stack
	PUTS R2, 2							// restore X register
	PUTS R1, 1							// restore H register
	LDW R15, R14, 13*4					// load R15 register from stack
	LDW R12, R14, 12*4					// load R12 register from stack
	LDW R11, R14, 11*4					// load R11 register from stack
	LDW R10, R14, 10*4					// load R10 register from stack
	LDW R9, R14, 9*4					// load R9 register from stack
	LDW R8, R14, 8*4					// load R8 register from stack
	LDW R7, R14, 7*4					// load R7 register from stack
	LDW R6, R14, 6*4					// load R6 register from stack
	LDW R5, R14, 5*4					// load R5 register from stack
	LDW R4, R14, 4*4					// load R4 register from stack
	LDW R3, R14, 3*4					// load R3 register from stack
	LDW R2, R14, 2*4					// load R2 register from stack
	LDW R1, R14, 1*4					// load R1 register from stack
	LDW R0, R14, 0*4					// load R0 register from stack
	
	CLI
	LDW R13, R14, 16*4					// load PSW from stack
	ADD R14, R14, contextFrameSize		// restore the old stack pointer
	PUTS R13, 3							// restore PSW
	RTI									// HACK: using RTI so we do not need to simulate hardware behaviour	+ atomic I flag restoring and return to address from X resgister	

///////////////////////////////////////////////////////////////////////////////////////
//
// void vPortYieldFromTick(void)
//
///////////////////////////////////////////////////////////////////////////////////////

vPortYieldFromTick:
	SUB R14, R14, 1*4
	STW R15, R14, 0*4					// save R15 because of the call in next instruction
	C vDisableLowPriorityInterrupts
	STI

	MOV R0, 0xFFFFC0					// read ms timer to clear interrupt
	LDW R0, R0, 0
	
	C xTaskIncrementTick				// increment tick
	LDW R15, R14, 0*4					// restore R15 
	ADD R14, R14, 1*4

	SUB R12, R0, 0						// check if scheduler needs to be called
	BEQ SkipTaskSwitch					// skip scheduler call if xTaskIncrementTick returned 0
	
	MOV R0, pxCurrentTCB				// get pointer to pxCurrentTCB
	LDW R0, R0, 0						// load pxCurrentTCB
	STW R14, R0, 0						// store stack pointer in pxCurrentTCB->pxTopOfStack

	SUB R14, R14, 1*4
	STW R15, R14, 0*4					// save R15 because of the call in next instruction
	C vTaskSwitchContext				// call scheduler
	LDW R15, R14, 0*4					// restore R15 
	ADD R14, R14, 1*4

	MOV R0, pxCurrentTCB				// get pointer to pxCurrentTCB
	LDW R0, R0, 0						// load pxCurrentTCB
	LDW R14, R0, 0						// store pxCurrentTCB->pxTopOfStack in stack pointer
SkipTaskSwitch:
	B R15								// return to TIMER_ISR

///////////////////////////////////////////////////////////////////////////////////////
//
// void vIrqDispatcher(void)
//
// Dispatch hardware interrupts to their corresponding ISRs installed in the 
// vector table risc5_ivt. This is FreeRTOS aware and replaces the default 
// dispatcher supplied by the RISC5 toolchain. 
//
// NOTE: This is technically not a function and must never be called directly.
//
///////////////////////////////////////////////////////////////////////////////////////

vIrqDispatcher:
	GETS R13, 3							// first of all save the PSW, because of the flags
	SUB	R14, R14, contextFrameSize		// calculate the new stack pointer for saving context
	STW R0, R14, 0*4					// save R0
	STW R1, R14, 1*4					// save R1
	STW R2, R14, 2*4					// save R2
	STW R3, R14, 3*4					// save R3
	STW R4, R14, 4*4					// save R4
	STW R5, R14, 5*4					// save R5
	STW R6, R14, 6*4					// save R6
	STW R7, R14, 7*4					// save R7
	STW R8, R14, 8*4					// save R8
	STW R9, R14, 9*4					// save R9
	STW R10, R14, 10*4					// save R10
	STW R11, R14, 11*4					// save R11
	STW R12, R14, 12*4					// save R12
	STW R15, R14, 13*4					// save R15
	GETS R1, 1							// get H register
	GETS R2, 2							// get X register
	STW R1, R14, 14*4					// save H register
	STW R2, R14, 15*4					// save X register
	STW R13, R14, 16*4					// save PSW

	ROR R13, R13, 16					// shift ACK in R13
	AND R13, R13, 0xF					// mask 4 LSBs of ACK
	LSL R13, R13, 2						// multiply by 4
	LDW R13, R13, risc5_ivt				// load address of ISR from risc5_ivt
	
	C R13								// call ISR
	CLI									// enter critical section

	LDW R13, R14, 16*4					// load PSW from stack
	LDW R2, R14, 15*4					// load X register from stack
	LDW R1, R14, 14*4					// load H register from stack
	PUTS R2, 2							// restore X register
	PUTS R1, 1							// restore H register
	LDW R15, R14, 13*4					// load R15 register from stack
	LDW R12, R14, 12*4					// load R12 register from stack
	LDW R11, R14, 11*4					// load R11 register from stack
	LDW R10, R14, 10*4					// load R10 register from stack
	LDW R9, R14, 9*4					// load R9 register from stack
	LDW R8, R14, 8*4					// load R8 register from stack
	LDW R7, R14, 7*4					// load R7 register from stack
	LDW R6, R14, 6*4					// load R6 register from stack
	LDW R5, R14, 5*4					// load R5 register from stack
	LDW R4, R14, 4*4					// load R4 register from stack
	LDW R3, R14, 3*4					// load R3 register from stack
	LDW R2, R14, 2*4					// load R2 register from stack
	LDW R1, R14, 1*4					// load R1 register from stack
	LDW R0, R14, 0*4					// load R0 register from stack
	ADD R14, R14, contextFrameSize		// restore the old stack pointer
	PUTS R13, 3							// restore PSW
	RTI