def main():
    with open('test.map', 'r') as map_file, open('test2.instr', 'r') as instr_file:
        instr_string = instr_file.read()
        for line in map_file:
            line_array = line.split()
            if len(line_array) >= 2:
                # instr_string = instr_string.replace('C       ' + line_array[1], 'C       ' + line_array[0])
                if line_array[0] not in ['risc5_ivt', 'risc5_start_irq_dispatch', '_bcode']:
                    instr_string = instr_string.replace(line_array[1], line_array[0])
        print(instr_string)


if __name__ == '__main__':
    main()
