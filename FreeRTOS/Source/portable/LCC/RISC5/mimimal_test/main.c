#include "FreeRTOS.h"
#include "task.h"

#include <stdio.h>
#include <risc5.h>

void vDummyTask(void *pvParameters);

void putchar(char c) {
	if (c == '\n') {
		putchar('\r');
	}
	while ((*RISC5_IO_RS232_0_STAT_CTRL & 2) == 0) ;
	*RISC5_IO_RS232_0_DATA = c;
}

int main(void)
{
	xTaskCreate(vDummyTask, "Dummy", configMINIMAL_STACK_SIZE, NULL, 1, NULL);
	vTaskStartScheduler();

	configASSERT(0);

	return 0;
}

void vDummyTask(void *pvParameters)
{
	int cnt = 0;
	( void ) pvParameters;

	for(;;)
	{
		printf("cnt: %d\n", cnt);
		vTaskDelay( pdMS_TO_TICKS(1000) );
		cnt++;
	}
}

void vAssertCalled( const char *fileName, int line )
{
	volatile unsigned int resume = 0;

	( void ) fileName;
	( void ) line;

	taskENTER_CRITICAL();
	{
		/* Set resume to a non-zero value using the debugger to step out of this
		function. */
		while( !resume )
		{
			portNOP();
		}
	}
	taskEXIT_CRITICAL();
}
