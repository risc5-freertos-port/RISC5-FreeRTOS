/*
 * FreeRTOS Kernel <DEVELOPMENT BRANCH>
 * Copyright (C) 2021 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * SPDX-License-Identifier: MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * https://www.FreeRTOS.org
 * https://github.com/FreeRTOS
 *
 */

#include <risc5.h>
#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"

/*----------------------------------------------------------------------------
 * Global variables
 *--------------------------------------------------------------------------*/

/*
 * This mask is used to disable all interrupts that are not allowed to
 * interrupt OS operations.
 *
 * NOTE: This is really a compile time constant, but the only way to 
 * access it from RISC5 assembly code is to place it in memory.
 */
const UBaseType_t portDisableIRQMask = (((UBaseType_t) ~0) << ((configMAX_API_CALL_INTERRUPT_PRIORITY) + 1));

/*----------------------------------------------------------------------------
 * Extern functions and variables defined in portasm.s
 *--------------------------------------------------------------------------*/

extern void vPortYieldFromTick( void );
extern vPortRestoreContext( void );
extern uint32_t vIrqDispatcher;
extern uint32_t contextFrameSize;

/*----------------------------------------------------------------------------
 * Static function declarations.
 *--------------------------------------------------------------------------*/

static void TIMER_ISR(void);

/*----------------------------------------------------------------------------
 * Implementation of functions defined in portable.h for the RISC5/LCC port.
 *--------------------------------------------------------------------------*/

/*
 * See portable.h file for a description of pxPortInitialiseStack, xPortStartScheduler, vPortEndScheduler.
 */

StackType_t *pxPortInitialiseStack( StackType_t *pxTopOfStack, TaskFunction_t pxCode, void *pvParameters )
{
	/*
	* Place known values at the bottom of the stack for debugging purposes
	*/
	pxTopOfStack--;
	*pxTopOfStack = 0x11111111;
	pxTopOfStack--;
	*pxTopOfStack = 0x22222222;
	pxTopOfStack--;
	*pxTopOfStack = 0x33333333;

	// Open Context Frame
	pxTopOfStack -= (((uint32_t)&contextFrameSize) / 4);

	pxTopOfStack[1] = (StackType_t)pvParameters;	// Place param pointer of first task into context frame -> will be restored into R1
	pxTopOfStack[15] = (StackType_t)pxCode;			// Place start address of first task into context frame -> will be restored as the return address
	pxTopOfStack[16] = (StackType_t) 0x0400FFFF;	// Place initial PSW contents into context frame -> will be restored into PSW

	pxTopOfStack[0] = (StackType_t) 0x30522020;
	pxTopOfStack[2] = (StackType_t) 0x32522020;
	pxTopOfStack[3] = (StackType_t) 0x33522020;
	pxTopOfStack[4] = (StackType_t) 0x34522020;
	pxTopOfStack[5] = (StackType_t) 0x35522020;
	pxTopOfStack[6] = (StackType_t) 0x36522020;
	pxTopOfStack[7] = (StackType_t) 0x37522020;
	pxTopOfStack[8] = (StackType_t) 0x38522020;
	pxTopOfStack[9] = (StackType_t) 0x39522020;
	pxTopOfStack[10] = (StackType_t) 0x30315220;
	pxTopOfStack[11] = (StackType_t) 0x31315220;
	pxTopOfStack[12] = (StackType_t) 0x32315220;
	pxTopOfStack[13] = (StackType_t) 0x33315220;
	pxTopOfStack[14] = (StackType_t) 0x34315220;

	return pxTopOfStack;
}

BaseType_t xPortStartScheduler( void )
{
	// install a FreeRTOS specific interrupt dispatcher
	RISC5_SET_IRQ_DISPATCHER(&vIrqDispatcher);

	// install timer ISR into IVT
#if configUSE_PREEMPTION == 0
	risc5_ivt[RISC5_IRQ_MSTMR] = TIMER_ISR;
#else
	risc5_ivt[RISC5_IRQ_MSTMR] = vPortYieldFromTick;
#endif

	// Enable ms timer interrupt
	*RISC5_IO_MSTMR = 1;

	/* Restore the context of the first task that is going to run.
	Normally we would just call portRESTORE_CONTEXT() here, but as the IAR
	compiler does not fully support inline assembler we have to make a call.*/
	vPortRestoreContext();

	/* Should not get here! */
	return pdTRUE;
}

void vPortEndScheduler( void )
{
	// Stopping the scheduler is currently unsupported.
	// If required, simply disable the tick interrupt here.
}

/*----------------------------------------------------------------------------
 * Static function definitions.
 *--------------------------------------------------------------------------*/

/*
 * In non-preemptive mode, we still need to advance the scheduler's
 * internal tick count on every MS-Timer tick. 
 * See `vPortYieldFromTick` in `portasm.h` for the full MS-Timer ISR 
 * implementation used in preemptive mode.
 */
#if configUSE_PREEMPTION == 0
void TIMER_ISR(void)
{
	vDisableLowPriorityInterrupts();
	_asm_STI();
	// read timer counter to reset interrupt
	uint32_t timer_value = *RISC5_IO_MSTMR;

	xTaskIncrementTick();
}
#endif
