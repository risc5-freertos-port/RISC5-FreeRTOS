#ifndef _HP_TIMER_H
#define _HP_TIMER_H

//------------------------------------------------------------------------------
//      Definitions
//------------------------------------------------------------------------------

#define MS_TIMER_DATA_ADDRESS       0xFFFFC0
#define MS_TIM_I_ENABLE             0X00000001
#define MS_TIM_I_DISABLE            0X00000000

//------------------------------------------------------------------------------
//         Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//         Exported functions
//------------------------------------------------------------------------------

unsigned long MS_Timer_Get_Counter( void );
void MS_Timer_Enable_Interrupt( void );
void MS_Timer_Disable_Interrupt( void );

#endif // _HP_TIMER_H
