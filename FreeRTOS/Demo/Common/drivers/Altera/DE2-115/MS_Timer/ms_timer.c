//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

#include "ms_timer.h"

//------------------------------------------------------------------------------
//         Local definitions
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//         Local variables
//------------------------------------------------------------------------------

static unsigned long * data_register = (unsigned long *)  MS_TIMER_DATA_ADDRESS;

//------------------------------------------------------------------------------
//         Local functions
//------------------------------------------------------------------------------

/// @brief Returns the current counter state
/// @param  none 
/// @return current counter state
unsigned long MS_Timer_Get_Counter( void )
{
    return *data_register;
}

/// @brief Enables the device interrupt 
/// @param  none 
void MS_Timer_Enable_Interrupt( void )
{
    *data_register = MS_TIM_I_ENABLE;
}

/// @brief Disables the device interrupt
/// @param  none 
void MS_Timer_Disable_Interrupt( void )
{
    *data_register = MS_TIM_I_DISABLE;
}
