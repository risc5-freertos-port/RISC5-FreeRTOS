#ifndef _HP_TIMER_H
#define _HP_TIMER_H

//------------------------------------------------------------------------------
//      Definitions
//------------------------------------------------------------------------------

#define HPTMR_0_DATA_ADDRESS       0xFFFF80
#define HPTMR_0_CTRL_ADDRESS       0xFFFF84
#define HPTMR_0_I_ENABLE           0x000001
#define HPTMR_0_I_DISABLE          0x000000

#define HPTMR_1_DATA_ADDRESS       0xFFFF98
#define HPTMR_1_CTRL_ADDRESS       0xFFFF9C
#define HPTMR_1_I_ENABLE           0x000001
#define HPTMR_1_I_DISABLE          0x000000

//------------------------------------------------------------------------------
//         Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//         Exported functions
//------------------------------------------------------------------------------

void HPTMR_0_Init( unsigned long divisor );
unsigned long HPTMR_0_Get_Counter( void );
void HPTMR_0_Enable_Interrupt( void );
void HPTMR_0_Disable_Interrupt( void );
void HPTMR_0_Reset_Interrupt_Flag( void );

void HPTMR_1_Init( unsigned long divisor );
unsigned long HPTMR_1_Get_Counter( void );
void HPTMR_1_Enable_Interrupt( void );
void HPTMR_1_Disable_Interrupt( void );
void HPTMR_1_Reset_Interrupt_Flag( void );

#endif // _HP_TIMER_H
