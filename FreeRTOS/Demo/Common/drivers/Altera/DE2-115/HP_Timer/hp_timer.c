//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

#include "hp_timer.h"

//------------------------------------------------------------------------------
//         Local definitions
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//         Local variables
//------------------------------------------------------------------------------

static unsigned long * tim0_data_register = (unsigned long *)  HPTMR_0_DATA_ADDRESS;
static unsigned long * tim0_ctrl_register = (unsigned long *)  HPTMR_0_CTRL_ADDRESS;
static unsigned long * tim1_data_register = (unsigned long *)  HPTMR_1_DATA_ADDRESS;
static unsigned long * tim1_ctrl_register = (unsigned long *)  HPTMR_1_CTRL_ADDRESS;

//------------------------------------------------------------------------------
//         Local functions
//------------------------------------------------------------------------------

/// @brief  Initializes High Precision Timer 0 with a given divisor.
///         The timer has a rate of 50 MHz (resolution = 20 ns) and counts
///         from the value of divisor to down to zero.
///         Restarts automatically.
/// @param  divisor is the starting point of the timer from which it gets decremented
///         down to 0.           
void HPTMR_0_Init( unsigned long divisor )
{
    *tim0_data_register = divisor;
}

/// @brief  Returns the current counter value of HPTMR_0
/// @param  none
/// @return counter value of HPTMR_0
unsigned long HPTMR_0_Get_Counter( void )
{
    return *tim0_data_register;
}

/// @brief Enables the device interrupt of HPTMR_0
/// @param none
void HPTMR_0_Enable_Interrupt( void )
{
    *tim0_ctrl_register = HPTMR_0_I_ENABLE;
}

/// @brief Disables the device interrupt of HPTMR_0
/// @param none
void HPTMR_0_Disable_Interrupt( void )
{
    *tim0_ctrl_register = HPTMR_0_I_DISABLE;
}

/// @brief Resets the Interrupt flag so another Interrupt can occure
/// @param none
void HPTMR_0_Reset_Interrupt_Flag( void )
{
    unsigned long ctrl = *tim0_ctrl_register;
}

/// @brief  Initializes High Precision Timer 1 with a given divisor.
///         The timer has a rate of 50 MHz (resolution = 20 ns) and counts
///         from the value of divisor to down to zero.
///         Restarts automatically.
/// @param  divisor is the starting point of the timer from which it gets decremented
///         down to 0. 
void HPTMR_1_Init( unsigned long divisor )
{
    *tim1_data_register = divisor;
}

/// @brief  Returns the current counter value of HPTMR_1
/// @param none
/// @return counter value of HPTMR_1
unsigned long HPTMR_1_Get_Counter( void )
{
    return *tim1_data_register;
}

/// @brief Enables the device interrupt of HPTMR_1
/// @param none
void HPTMR_1_Enable_Interrupt( void )
{
    *tim1_ctrl_register = HPTMR_1_I_ENABLE;
}

/// @brief Disables the device interrupt of HPTMR_1
/// @param none
void HPTMR_1_Disable_Interrupt( void )
{
    *tim1_ctrl_register = HPTMR_1_I_DISABLE;
}

/// @brief Resets the Interrupt flag so another Interrupt can occure
/// @param none
void HPTMR_1_Reset_Interrupt_Flag( void )
{
    unsigned long ctrl = *tim1_ctrl_register;
}
