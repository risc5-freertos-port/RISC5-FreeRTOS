#include "../SPI/spi.h"
#include "./sdc.h"

/// @brief Send n FFs slowly with no card selected
/// @param n Number of idle cycles 
static void SDC_Idle( int n ) 
{
    SPI_Init( 0 );
    while( n > 0 ) 
    {
        n--;
        SPI_Write( ( int ) 0xFFFFFFFF );
        while( ( SPI_Rdy() ) == 0 );
    }
}

/// @brief Send & receive word slowly with card selected
/// @param n Word to be written
static void SDC_Write_Word( int n ) 
{
    SPI_Init( SPI_SDC_SEL );
    SPI_Write( n );
    while( ( SPI_Rdy() ) == 0 );
}

/// @brief Send command
/// @param n 
/// @param arg 
static void SDC_Cmd( int n, int arg ) 
{
    int data;
    int crc;
    int i;

    /* flush while deselected */
    do 
    {
        SDC_Idle( 1 );
        data = SPI_Read();
    } while( data != 255 );
    /* flush while selected */
    do 
    {
        SDC_Write_Word( 255 );
        data = SPI_Read();
    }while( data != 255 );
    /* select precomputed CRC */
    crc = ( n == 8 ) ? 135 : ( ( n == 0 ) ? 149 : 255 );
    /* send command */
    SDC_Write_Word( 0x40 | ( n & 0x3F ) );
    /* send arg */
    for( i = 24; i >= 0; i -= 8 ) 
    {
        SDC_Write_Word( arg >> i );
    }
    /* send CRC */
    SDC_Write_Word( crc );
    /* wait for response */
    i = 32;
    do 
    {
        SDC_Write_Word( 255 );
        data = SPI_Read();
        i--;
    }while( data >= 0x80 && i != 0 );
}

/// @brief 
/// @param p 
static void SDC_Shift( unsigned int *p ) 
{
    int data;

    /* CMD58: get card capacity bit */
    SDC_Cmd( 58, 0 );
    data = SPI_Read();
    SDC_Write_Word( -1 );
    if ( data != 0 || ( SPI_Read() & 0x40 ) == 0 ) 
    {
        *p *= 512;
    }
    SDC_Write_Word( -1 );
    SDC_Write_Word( -1 );
    SDC_Idle( 1 );
}

/// @brief Read block from SD-Card
/// @param src Block address of SD-Card
/// @param dst Block address of the board
/// @param count Number of blocks to read
void SDC_Read( unsigned int src, unsigned int * dst, unsigned int count ) 
{
    unsigned int data;
    unsigned int i;
    unsigned int j;

    for( j = 0; j < count; ++j )
    {
        SDC_Shift( &src );
        /* CMD17: read one block */
        SDC_Cmd( 17, src );
        /* wait for start data marker */
        do 
        {
            SDC_Write_Word( -1 );
            data = SPI_Read();
        }while( data != 254 );
        /* switch to fast */
        SPI_Init( SPI_FAST + SPI_SDC_SEL );
        /* get data */
        for ( i = 0; i <= ( SDC_BLOCK_SIZE - SDC_WORD_SIZE ); i += SDC_WORD_SIZE ) 
        {
            SPI_Write( ( int ) 0xFFFFFFFF );
            while( SPI_Rdy() == 0 );
            data = SPI_Read();
            * dst = data;
            ++dst;
        }
        /* may be a checksum */
        SDC_Write_Word( 255 );
        SDC_Write_Word( 255 );
        /* deselect card */
        SDC_Idle( 1 );
        ++src;
    }
}

/// @brief Write block to SD-Card
/// @param dst Block address of SD-Card
/// @param src Block address of the board
/// @param count Number of blocks to write
void SDC_Write( unsigned int dst, unsigned int * src, unsigned int count ) 
{
    unsigned int data;
    unsigned int i;
    unsigned int j;

    for( j = 0; j < count; ++j )
    {
        SDC_Shift( &dst );
        /* CMD24: write one block */
        SDC_Cmd( 24, dst );
        /* write start data marker */
        SDC_Write_Word( 254 );
        /* switch to fast */
        SPI_Init( SPI_FAST + SPI_SDC_SEL );
        for( i = 0; i <= ( SDC_BLOCK_SIZE - SDC_WORD_SIZE ); i += SDC_WORD_SIZE ) 
        {
            data = * src;
            ++src;
            SPI_Write( data );
            while( SPI_Rdy() == 0);
        }
        /* dummy checksum */
        SDC_Write_Word( 255 );
        SDC_Write_Word( 255 );
        i = 0;
        do 
        {
            SDC_Write_Word( -1 );
            data = SPI_Read();
            i++;
        }while( ( data & 31 ) != 5 && i != 10000 );
        /* deselect card */
        SDC_Idle( 1 );
        ++dst;
    }
}

/// @brief Write block to SD-Card
/// @return Number of Blocks
unsigned long SDC_Capacity( void ) 
{
  unsigned long data;
  int i;
  unsigned char csd[16];
  unsigned long csize;
  unsigned long numSectors;

  /* CMD9: send card-specific data (CSD) */
  SDC_Cmd( 9, 0 );
  /* wait for start data marker */
  do 
  {
    SDC_Write_Word( -1 );
    data = * ( unsigned int * ) SPI_DATA_ADDRESS;
  } while ( data != 254 );
  /* get data */
  for ( i = 0; i < 16; i++ ) {
    * ( unsigned int * ) SPI_DATA_ADDRESS = 0xFFFFFFFF;
    while ( (* ( unsigned int * ) SPI_STATUS_ADDRESS & 1 ) == 0 ) ;
    data = * ( unsigned int * ) SPI_DATA_ADDRESS;
    csd[i] = data;
  }
  /* may be a checksum */
  SDC_Write_Word( 255 );
  SDC_Write_Word( 255 );
  /* deselect card */
  SDC_Idle( 1 );
  /* verify correct CSD version */
  if ( ( csd[0] & 0xC0 ) != 0x40 ) 
  {
    /* wrong CSD structure version */
    return 0;
  }
  /* extract size information */
  csize = ( ( ( unsigned long ) csd[7] & 0x3F ) << 16 ) |
          ( ( ( unsigned long ) csd[8] & 0xFF ) <<  8 ) |
          ( ( ( unsigned long ) csd[9] & 0xFF ) <<  0 );
  /* compute number of sectors */
  numSectors = ( csize + 1 ) << 10;
  return numSectors;
}
