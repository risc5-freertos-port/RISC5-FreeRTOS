#ifndef _SDC_H
#define _SDC_H

//------------------------------------------------------------------------------
//      Definitions
//------------------------------------------------------------------------------

#define SDC_BLOCK_SIZE 512
#define SDC_WORD_SIZE  4    

//------------------------------------------------------------------------------
//         Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//         Exported functions
//------------------------------------------------------------------------------

void SDC_Read( unsigned int src, unsigned int * dst, unsigned int count );
void SDC_Write( unsigned int dst, unsigned int * src, unsigned int count );
unsigned long SDC_Capacity( void );

#endif // _SDC_H

