#ifndef _LEDS_H
#define _LEDS_H

//------------------------------------------------------------------------------
//      Definitions
//------------------------------------------------------------------------------

#define LED_DATA_ADDRESS            0xFFFFC4

#define LED_00		        	    (unsigned char) 0x01
#define LED_01		        	    (unsigned char) ( 0x01 << 1 )
#define LED_02				        (unsigned char) ( 0x01 << 2 )
#define LED_03		        	    (unsigned char) ( 0x01 << 3 )
#define LED_04		        	    (unsigned char) ( 0x01 << 4 )
#define LED_05				        (unsigned char) ( 0x01 << 5 )
#define LED_06		        	    (unsigned char) ( 0x01 << 6 )
#define LED_07		        	    (unsigned char) ( 0x01 << 7 )
#define LED_ALL				        (unsigned char) 0xFF
#define LED_NONE				    (unsigned char) 0x00


//------------------------------------------------------------------------------
//         Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//         Exported functions
//------------------------------------------------------------------------------

extern void LEDs_Write( unsigned char data );

#endif // _LEDS_H
