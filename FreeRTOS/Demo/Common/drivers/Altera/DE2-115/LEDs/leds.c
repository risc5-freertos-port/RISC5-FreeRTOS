//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

#include "leds.h"

//------------------------------------------------------------------------------
//         Local definitions
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//         Local variables
//------------------------------------------------------------------------------

static unsigned long * data_register = (unsigned long *)  LED_DATA_ADDRESS;

//------------------------------------------------------------------------------
//         Local functions
//------------------------------------------------------------------------------

/// @brief Writes the led bit pattern at the LED_DATA_ADDRESS. 
///        Overwrites the last led bit pattern completely. 
/// @param data format: [7:0]. Each bit represents one led.
///             1 is led on, 0 is led off
extern void LEDs_Write( unsigned char data )
{
    *data_register = data & 0xFF;
}
