#ifndef _COMMON_DE2_115_H
#define _COMMON_DE2_115_H_RS232_H

//------------------------------------------------------------------------------
//      Definitions
//------------------------------------------------------------------------------

#define COMMON_DE2_115_0000     0x0
#define COMMON_DE2_115_0001     0x1
#define COMMON_DE2_115_0010     0x2
#define COMMON_DE2_115_0011     0x3
#define COMMON_DE2_115_0100     0x4
#define COMMON_DE2_115_0101     0x5
#define COMMON_DE2_115_0110     0x6
#define COMMON_DE2_115_0111     0x7
#define COMMON_DE2_115_1000     0x8
#define COMMON_DE2_115_1001     0x9
#define COMMON_DE2_115_1010     0xA
#define COMMON_DE2_115_1011     0xB
#define COMMON_DE2_115_1100     0xC
#define COMMON_DE2_115_1101     0xD
#define COMMON_DE2_115_1110     0xE
#define COMMON_DE2_115_1111     0xF

//------------------------------------------------------------------------------
//         Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//         Exported functions
//------------------------------------------------------------------------------

#endif // _COMMON_DE2_115_H

