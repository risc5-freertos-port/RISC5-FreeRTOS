//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

#include "rs232.h"
#include "../Common_DE2_115.h"

//------------------------------------------------------------------------------
//         Local definitions
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//         Local variables
//------------------------------------------------------------------------------
static volatile unsigned long * status_register_0 = (unsigned long *) RS232_0_STATUS_ADDRESS;
static volatile unsigned long * data_register_0 = (unsigned long *) RS232_0_DATA_ADDRESS;
static volatile unsigned long * status_register_1 = (unsigned long *) RS232_1_STATUS_ADDRESS;
static volatile unsigned long * data_register_1 = (unsigned long *) RS232_1_DATA_ADDRESS;
static unsigned long i_flag_mask = 0;

//------------------------------------------------------------------------------
//         Local functions
//------------------------------------------------------------------------------

/// @brief  Initialises the RS232 with a given baudrate.
/// @param  baudrate in format RS232_BAUD_RATE_XXXX where the rate can 
///         be 2400, 4800, 9600, 19200, 31250, 38400, 57600,115200
/// @param  rs232_nbr choose between devices with RS232_0 or RS232_1
/// @return RS232_OK on success, RS232_FAIL if wrong baudrate was chosen
extern int RS232_Init( unsigned int baudrate, int rs232_nbr)
{
    while( RS232_XMT_Empty( rs232_nbr ) != RS232_XMT_EMPTY );
    // check if bausrate is valid
    switch( baudrate )
    {
        case    RS232_BAUD_RATE_2400: 
                    break;
        case    RS232_BAUD_RATE_4800: 
                    break;
        case    RS232_BAUD_RATE_9600: 
                    break;
        case    RS232_BAUD_RATE_19200: 
                    break;
        case    RS232_BAUD_RATE_31250: 
                    break;
        case    RS232_BAUD_RATE_38400: 
                    break;
        case    RS232_BAUD_RATE_57600: 
                    break;
        case    RS232_BAUD_RATE_115200: 
                    break;
        default: 
                    return RS232_FAIL;
    }
    // set baudrate
    switch( rs232_nbr )
    {
        case RS232_0:
            *status_register_0 = baudrate | RS232_SET_BAUD;
            break;
        case RS232_1:
            *status_register_1 = baudrate | RS232_SET_BAUD;
            break;
        default:
            return RS232_FAIL;
    }
    
    return RS232_OK;
}

/// @brief  Writes data to TX buffer, only be used in ISR
/// @param  data byte to be written
/// @param  rs232_nbr choose between devices with RS232_0 or RS232_1
extern void RS232_Write_i( char data, int rs232_nbr )
{
    switch( rs232_nbr )
    {
        case RS232_0:
            *data_register_0 = data;
            break;
        case RS232_1:
            *data_register_1 = data;
            break;
    }
}

/// @brief  Reads data from RX buffer, only be used in ISR
/// @param  rs232_nbr choose between devices with RS232_0 or RS232_1
/// @return byte which was received
extern char RS232_Read_i( int rs232_nbr )
{   
    switch( rs232_nbr )
    {
        case RS232_0:
            return (char)(*data_register_0 & RS232_BYTE_MASK);
        case RS232_1:
            return (char)(*data_register_1 & RS232_BYTE_MASK);
        default:
            return RS232_FAIL;
    }
}

/// @brief  Returns information about receive state
/// @param  rs232_nbr choose between devices with RS232_0 or RS232_1
/// @return RS232_RCV_RDY if ready to read byte
extern int RS232_RCV_Ready( int rs232_nbr )
{
    switch( rs232_nbr )
    {
        case RS232_0:
            return *status_register_0 & RS232_RCV_RDY;
        case RS232_1:
            return *status_register_1 & RS232_RCV_RDY;
        default:
            return RS232_FAIL;
    }
}

/// @brief  Returns information about transmit state
/// @param  rs232_nbr choose between devices with RS232_0 or RS232_1
/// @return RS232_XMT_RDY if ready to write byte
extern int RS232_XMT_Ready( int rs232_nbr )
{
    switch( rs232_nbr )
    {
        case RS232_0:
            return *status_register_0 & RS232_XMT_RDY;
        case RS232_1:
            return *status_register_1 & RS232_XMT_RDY;
        default:
            return RS232_FAIL;
    }
}

/// @brief  Returns information about transmit buffer state
/// @param  rs232_nbr choose between devices with RS232_0 or RS232_1
/// @return RS232_RCV_EMPTY if transmit buffer is empty
extern int RS232_XMT_Empty( int rs232_nbr )
{
    switch( rs232_nbr )
    {
        case RS232_0:
            return *status_register_0 & RS232_XMT_EMPTY;
        case RS232_1:
            return *status_register_1 & RS232_XMT_EMPTY;
        default:
            return RS232_FAIL;
    }
}

/// @brief  Enables RS232 rcv device driver interrupt
/// @param  rs232_nbr choose between devices with RS232_0 or RS232_1
extern void RS232_RCV_Enable_Interrupt( int rs232_nbr )
{
    switch( rs232_nbr )
    {
        case RS232_0:
            i_flag_mask |= (unsigned long) RS232_SET_RCV_I_EN;
            *status_register_0 = i_flag_mask;
            break; 
        case RS232_1:
            i_flag_mask |= (unsigned long) RS232_SET_RCV_I_EN;
            *status_register_1 = i_flag_mask; 
            break;
        default:
            break;    
    } 
}

/// @brief  Enables RS232 xmt device driver interrupt
/// @param  rs232_nbr choose between devices with RS232_0 or RS232_1
extern void RS232_XMT_Enable_Interrupt( int rs232_nbr )
{
    switch( rs232_nbr )
    {
        case RS232_0:
            i_flag_mask |= (unsigned long) RS232_SET_XMT_I_EN;
            *status_register_0 = i_flag_mask;
            break; 
        case RS232_1:
            i_flag_mask |= (unsigned long) RS232_SET_XMT_I_EN;
            *status_register_1 = i_flag_mask;
            break;
        default:
            break;    
    } 
}

/// @brief  Disables RS232 rcv device driver interrupt
/// @param  rs232_nbr choose between devices with RS232_0 or RS232_1
extern void RS232_RCV_Disable_Interrupt( int rs232_nbr )
{
    switch( rs232_nbr )
    {
        case RS232_0:
            i_flag_mask &= ~(unsigned long) RS232_SET_RCV_I_EN;
            *status_register_0 = i_flag_mask;
            break; 
        case RS232_1:
            i_flag_mask &= ~(unsigned long) RS232_SET_RCV_I_EN;
            *status_register_1 = i_flag_mask;
            break;
        default:
            break;    
    } 
}

/// @brief  Disables RS232 xmt device driver interrupt
/// @param  rs232_nbr choose between devices with RS232_0 or RS232_1 
extern void RS232_XMT_Disable_Interrupt( int rs232_nbr )
{
    switch( rs232_nbr )
    {
        case RS232_0:
            i_flag_mask &= ~(unsigned long) RS232_SET_XMT_I_EN;
            *status_register_0 = i_flag_mask;
            break; 
        case RS232_1:
            i_flag_mask &= ~(unsigned long) RS232_SET_XMT_I_EN;
            *status_register_1 = i_flag_mask;
            break;
        default:
            break;    
    } 
}
