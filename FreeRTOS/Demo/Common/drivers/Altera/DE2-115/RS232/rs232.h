#ifndef _RS232_H
#define _RS232_H

//------------------------------------------------------------------------------
//      Definitions
//------------------------------------------------------------------------------

#define RS232_0_DATA_ADDRESS        0xFFFFC8
#define RS232_0_STATUS_ADDRESS      0xFFFFCC
#define RS232_1_DATA_ADDRESS        0xFFFFA0
#define RS232_1_STATUS_ADDRESS      0xFFFFA4

#define RS232_0                    0
#define RS232_1                    1

#define RS232_BYTE_MASK             0xFF

#define RS232_RCV_RDY		        0x1
#define RS232_XMT_RDY		        0x2
#define RS232_XMT_EMPTY		        0x4

#define RS232_OK                    0
#define RS232_FAIL                  -1

#define RS232_SET_RCV_I_EN           0x1
#define RS232_SET_XMT_I_EN           0x2

#define RS232_BAUD_POS              28
#define RS232_SET_BAUD_POS          31
#define RS232_SET_BAUD              (0x1 << RS232_SET_BAUD_POS)

#define RS232_BAUD_RATE_2400        (0x0 << RS232_BAUD_POS)
#define RS232_BAUD_RATE_4800        (0x1 << RS232_BAUD_POS)
#define RS232_BAUD_RATE_9600        (0x2 << RS232_BAUD_POS)
#define RS232_BAUD_RATE_19200       (0x3 << RS232_BAUD_POS)
#define RS232_BAUD_RATE_31250       (0x4 << RS232_BAUD_POS)
#define RS232_BAUD_RATE_38400       (0x5 << RS232_BAUD_POS)
#define RS232_BAUD_RATE_57600       (0x6 << RS232_BAUD_POS)
#define RS232_BAUD_RATE_115200      (0x7 << RS232_BAUD_POS)

//------------------------------------------------------------------------------
//         Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//         Exported functions
//------------------------------------------------------------------------------

extern int RS232_Init( unsigned int baudrate, int rs232_nbr );
extern void RS232_Write_i( char data, int rs232_nbr );
extern char RS232_Read_i( int rs232_nbr );
extern int RS232_RCV_Ready( int rs232_nbr );
extern int RS232_XMT_Ready( int rs232_nbr );
extern int RS232_XMT_Empty( int rs232_nbr );
extern void RS232_RCV_Enable_Interrupt( int rs232_nbr );
extern void RS232_XMT_Enable_Interrupt( int rs232_nbr );
extern void RS232_RCV_Disable_Interrupt( int rs232_nbr );
extern void RS232_XMT_Disable_Interrupt( int rs232_nbr );

#endif // _RS232_H

