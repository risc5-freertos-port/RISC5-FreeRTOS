#ifndef _LCD_H
#define _LCD_H

//------------------------------------------------------------------------------
//      Definitions
//------------------------------------------------------------------------------

#define LCD_DATA_ADDRESS                0xFFFF88
#define LCD_STATE_CTRL_ADDRESS          0xFFFF8C

#define LCD_DATA_MASK                   0x000000FF
#define LCD_STATE_CTRL_MASK             0x0000000F 

#define LCD_FIRST_LINE_ADDR             0x00
#define LCD_SECOND_LINE_ADDR            0x40

#define LCD_MAX_CHARS_SHOWN             16

// on en rw rs -> muss man alles selbst setzen
// rs 1:data, 0:instructions
// write data: rw=0,rs=0/1, en:1 ,en: 0
// read data: rw=1, rs= 0/1 je nachdem was man gelesen hat, bekommt man das entsprechende heraus 
// read data 
// rw und rs gleichzeitig setzbar 
#define LCD_RS                      0x1     // Register Selection Signal, unterscheidet zwischen Instruktions oder Daten register 
#define LCD_RW_READ                 (0x1 << 1)
#define LCD_EN                      (0x1 << 2)
#define LCD_ON                      (0x1 << 3)

// Mode Bits 
#define LCD_CLEAR_DISPLAY         0x1
#define LCD_RETURN_HOME           (0x1 << 1)
#define LCD_ENTRY_MODE_SET        (0x1 << 2)
#define LCD_DISPLAY_CONTROL       (0x1 << 3)
#define LCD_SHIFT                 (0x1 << 4)
#define LCD_FUNCTION_SET          (0x1 << 5)
#define LCD_SET_CGRAM_ADDRESS     (0x1 << 6)
#define LCD_SET_DDRAM_ADDRESS     (0x1 << 7)           

// How many bits the "bits to modify corresponding mode" must be shifted
/// Entry mode set
#define LCD_I_D                   1
#define LCD_S                     0 
/// Display on/off control 
#define LCD_D                     2
#define LCD_C                     1
#define LCD_B                     0
/// Cursor of Display Shift
#define LCD_S_C                   3
#define LCD_R_L                   2
/// Function set
#define LCD_DL                    4
#define LCD_N                     3
#define LCD_F                     2    
/// Busy Flag Position
#define LCD_BF                    7     

// Bits to modify corresponding mode 
#define LCD_I_D_INCREMENT         0x1
#define LCD_I_D_DECREMENT         0x0
#define LCD_S_ACC_DISPLAY_SHIFT   0x1        // Accompanies display shift
#define LCD_S_ACC_DISPLAY_SHIFT_NOT   0x0 
#define LCD_S_C_DISPLAY_SHIFT     0x1
#define LCD_S_C_CURSOR_MOVE       0x0
#define LCD_R_L_RIGHT_SHIFT       0x1
#define LCD_R_L_LEFT_SHIFT        0x0
#define LCD_DL_8_BITS             0x1
#define LCD_DL_4_BITS             0x0
#define LCD_N_TWO_LINES           0x1
#define LCD_N_ONE_LINE            0x0
#define LCD_F_FONT_1              0x1
#define LCD_F_FONT_0              0x0
#define LCD_D_DISPLAY_ON          0x1
#define LCD_D_DISPLAY_OFF         0x0
#define LCD_C_CURSOR_ON           0x1
#define LCD_C_CURSOR_OFF          0x0
#define LCD_B_BLINKING_ON         0x1
#define LCD_B_BLINKING_OFF        0x0

#define LCD_BF_BUSY        0x1     
#define LCD_BF_READY       0x0

// masks for mode -> some bits must be zero 
#define LCD_CLEAR_DISPLAY_MASK         0x1
#define LCD_RETURN_HOME_MASK           0x3
#define LCD_ENTRY_MODE_SET_MASK        0x7
#define LCD_DISPLAY_CONTROL_MASK       0xF
#define LCD_SHIFT_MASK                 0x1F
#define LCD_FUNCTION_SET_MASK          0x3F
#define LCD_SET_CGRAM_ADDRESS_MASK     0x7F
#define LCD_SET_DDRAM_ADDRESS_MASK     0xFF
#define LCD_ADDRESS_COUNTER_MASK       0x7F
#define LCD_BF_MASK                    0xFF

//------------------------------------------------------------------------------
//         Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//         Exported functions
//------------------------------------------------------------------------------

extern void LCD_Init( void );
extern void LCD_Write_Char ( unsigned char c );
extern void LCD_Write_String( unsigned char position, char* str, int str_len );
extern void LCD_Shift( unsigned char rl_shift, unsigned char sc );
extern void LCD_Set_DDRAM_Address ( unsigned char ddram_address );
extern void LCD_Set_Entry_Mode ( unsigned char ic, unsigned char sc );
extern unsigned char LCD_Read_Data ( void );
extern unsigned char LCD_Read_Busyflag( void );
extern unsigned char LCD_Read_Address_Counter( void ); 

extern void LCD_Write_State_Ctrl( unsigned char data );
extern void LCD_Write_Data ( unsigned char data );

extern unsigned char LCD_Create_Clear_Display( void );
extern unsigned char LCD_Create_Return_Home( void );
extern unsigned char LCD_Create_Entry_Mode_Set( unsigned char ic, unsigned char s_acc );
extern unsigned char LCD_Create_Display_Control( unsigned char display, unsigned char cursor, unsigned char blinking);
extern unsigned char LCD_Create_Shift( unsigned char sc, unsigned char rl_shift );
extern unsigned char LCD_Create_Function_Set( unsigned char dl_bits, unsigned char n_line, unsigned char font  );
extern unsigned char LCD_Create_Set_CGRAM_Address ( unsigned char cgram_address );
extern unsigned char LCD_Create_Set_DDRAM_Address ( unsigned char ddram_address );


extern void LCD_Delay( unsigned long counter );
extern void LCD_Wait_For_Busyflag( void );

#endif // _LCD_H
