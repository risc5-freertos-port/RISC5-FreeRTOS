//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

#include "lcd.h"

//------------------------------------------------------------------------------
//         Local definitions
//------------------------------------------------------------------------------
#define IO_BASE 0xFFFFC0
//------------------------------------------------------------------------------
//         Global variables
//------------------------------------------------------------------------------

static unsigned long * data_register = (unsigned long *) LCD_DATA_ADDRESS;
static unsigned long * state_ctrl_register = (unsigned long *) LCD_STATE_CTRL_ADDRESS;


//------------------------------------------------------------------------------
//         Local functions
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//         Global functions
//------------------------------------------------------------------------------

//-----------------------------------------
// these functions can be used directly by user  
//-----------------------------------------


/// @brief Initializes the LCD. Must be called, before the LCD is used.
/// @param none 
extern void LCD_Init( void )
{
    LCD_Write_State_Ctrl(LCD_ON); // 8 
    // power on init 
    LCD_Delay(40000);
    LCD_Write_Data(LCD_Create_Function_Set(LCD_DL_8_BITS, LCD_N_ONE_LINE, LCD_F_FONT_0)); // 30
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN); // c
    LCD_Write_State_Ctrl(LCD_ON); // 8
    LCD_Delay(4100);
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN); // c
    LCD_Write_State_Ctrl(LCD_ON); // 8
    LCD_Delay(100);
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN); // c
    LCD_Write_State_Ctrl(LCD_ON); // 8
    // function set
    LCD_Wait_For_Busyflag();
    LCD_Write_Data(LCD_Create_Function_Set(LCD_D_DISPLAY_ON, LCD_N_TWO_LINES, LCD_F_FONT_1)); // 38
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN); // c
    LCD_Write_State_Ctrl(LCD_ON); // 8
    // display off 
    LCD_Wait_For_Busyflag();
    LCD_Write_Data(LCD_Create_Display_Control(LCD_D_DISPLAY_OFF, LCD_C_CURSOR_OFF, LCD_B_BLINKING_OFF)); // 08
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN); // c
    LCD_Write_State_Ctrl(LCD_ON); // 8
    // display clear 
    LCD_Wait_For_Busyflag();
    LCD_Write_Data(LCD_Create_Clear_Display()); // 01
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN); // c
    LCD_Write_State_Ctrl(LCD_ON); // 8
    // entry mode set
    LCD_Wait_For_Busyflag();
    LCD_Set_Entry_Mode(LCD_I_D_INCREMENT, LCD_S_ACC_DISPLAY_SHIFT_NOT);
    // display on
    LCD_Wait_For_Busyflag();
    LCD_Write_Data(LCD_Create_Display_Control(LCD_D_DISPLAY_ON, LCD_C_CURSOR_OFF, LCD_B_BLINKING_OFF)); // 0c
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN); // c
    LCD_Write_State_Ctrl(LCD_ON); // 8
    LCD_Write_State_Ctrl(LCD_ON | LCD_RS); // 9
}

/// @brief writes a char to the current cursor position 
/// @param c char to write on the current cursor position 
extern void LCD_Write_Char ( unsigned char c)
{
    LCD_Wait_For_Busyflag();
    LCD_Write_Data(c);
    LCD_Write_State_Ctrl(LCD_ON | LCD_RS | LCD_EN); // d
    LCD_Write_State_Ctrl(LCD_ON | LCD_RS); // 9
}

/// @brief writes string to the given position 
/// @param position position to where the string should be written
/// @param str string that will be displayed 
extern void LCD_Write_String( unsigned char position, char* str, int str_len )
{
    int i;

    if(position < LCD_MAX_CHARS_SHOWN)
    {
        LCD_Set_DDRAM_Address(position);
    }
    else
    {
        LCD_Set_DDRAM_Address(position + LCD_SECOND_LINE_ADDR - LCD_MAX_CHARS_SHOWN);

    }
 
    for(i=0; i<str_len; ++i)
    {
        LCD_Write_Char(*str++);
    }
}

/// @brief Shift the cursor or move the complete display
/// @param rl_shift LCD_R_L_RIGHT_SHIFT or LCD_R_L_LEFT_SHIFT
/// @param sc LCD_S_C_DISPLAY_SHIFT or LCD_S_C_CURSOR_MOVE
extern void LCD_Shift(unsigned char rl_shift, unsigned char sc) 
{
    LCD_Wait_For_Busyflag();
    LCD_Write_Data(LCD_SHIFT | rl_shift | sc);
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN); // c
    LCD_Write_State_Ctrl(LCD_ON); // 8
}

/// @brief Sets the ddram address
/// @param ddram_address the ddram address to set in hex
extern void LCD_Set_DDRAM_Address (unsigned char ddram_address) 
{
    LCD_Wait_For_Busyflag();
    LCD_Write_Data(LCD_Create_Set_DDRAM_Address(ddram_address));
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN); // c
    LCD_Write_State_Ctrl(LCD_ON); // 8
}

/// @brief Sets the entry mode
/// @param ic LCD_I_D_INCREMENT or LCD_I_D_DECREMENT 
/// @param sc LCD_S_C_DISPLAY_SHIFT or LCD_S_C_CURSOR_MOVE  
extern void LCD_Set_Entry_Mode (unsigned char ic, unsigned char sc) 
{
    LCD_Wait_For_Busyflag();
    LCD_Write_Data(LCD_Create_Entry_Mode_Set(ic, sc)); 
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN); // c
    LCD_Write_State_Ctrl(LCD_ON); // 8
}

/// @brief Reads the char, which is currently on the cursor position
/// @param  none
/// @return the char, which is on the current cursor position
extern unsigned char LCD_Read_Data ( void )
{ 
    LCD_Wait_For_Busyflag();
    LCD_Write_State_Ctrl(LCD_ON | LCD_RS | LCD_EN | LCD_RW_READ);
    LCD_Write_State_Ctrl(LCD_ON | LCD_RS | LCD_RW_READ);
    return *data_register;
}

/// @brief Reads the busy flag 
/// @param none
/// @return 0 if not busy, 1 if busy 
extern unsigned char LCD_Read_Busyflag( void ) 
{
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN | LCD_RW_READ);
    LCD_Write_State_Ctrl(LCD_ON | LCD_RW_READ);
    return ((*data_register & LCD_BF_MASK) >> LCD_BF);
}

/// @brief reads the address counter 
/// @param none 
/// @return the address counter
extern unsigned char LCD_Read_Address_Counter( void ) 
{
    LCD_Write_State_Ctrl(LCD_ON | LCD_EN | LCD_RW_READ);
    LCD_Write_State_Ctrl(LCD_ON | LCD_RW_READ);
    return *data_register & LCD_ADDRESS_COUNTER_MASK;
}

//-----------------------------------------
// writes directly to registers
//-----------------------------------------

/// @brief writes the data to the data register
/// @param data the data to write 
extern void LCD_Write_Data ( unsigned char data )
{
    *data_register = data & LCD_DATA_MASK;
}

/// @brief writes the data to the state control register 
/// @param data the data to write
extern void LCD_Write_State_Ctrl( unsigned char data )
{
    *state_ctrl_register = data & LCD_STATE_CTRL_MASK;
}

//-----------------------------------------
// helper functions for bit code creation    
//-----------------------------------------

/// @brief Creates the bit code for clear display 
/// @param none
/// @return bit code for clear display 
extern unsigned char LCD_Create_Clear_Display( void )
{  
    return LCD_CLEAR_DISPLAY;
}

/// @brief Creates the bit code for return home 
/// @param none 
/// @return bit code for return home 
extern unsigned char LCD_Create_Return_Home( void )
{
    return LCD_RETURN_HOME;
}

/// @brief Creates the bit code for entry mode set 
/// @param ic LCD_I_D_INCREMENT or LCD_I_D_DECREMENT 
/// @param s_acc LCD_S_ACC_DISPLAY_SHIFT or LCD_S_ACC_DISPLAY_SHIFT_NOT 
/// @return bit code for entry mode set  
extern unsigned char LCD_Create_Entry_Mode_Set( unsigned char ic, unsigned char s_acc ) 
{
    unsigned char entry_mode_set = LCD_ENTRY_MODE_SET;
    entry_mode_set |= (ic << LCD_I_D);
    entry_mode_set |= (s_acc << LCD_S);
    return LCD_ENTRY_MODE_SET_MASK & entry_mode_set;
}

/// @brief Creates the bit code for display control 
/// @param display LCD_D_DISPLAY_ON or LCD_D_DISPLAY_OFF
/// @param cursor LCD_C_CURSOR_ON or LCD_C_CURSOR_OFF 
/// @param blinking LCD_B_BLINKING_ON or LCD_B_BLINKING_OFF
/// @return bit code for display control
extern unsigned char LCD_Create_Display_Control( unsigned char display, unsigned char cursor, unsigned char blinking ) 
{
    unsigned char display_control = LCD_DISPLAY_CONTROL;
    display_control |= display << LCD_D;
    display_control |= cursor << LCD_C;
    display_control |= blinking << LCD_B;
    return LCD_DISPLAY_CONTROL_MASK & display_control;
}

/// @brief Creates the bit code for shift
/// @param sc LCD_S_C_DISPLAY_SHIFT or LCD_S_C_CURSOR_MOVE
/// @param rl_shift LCD_R_L_RIGHT_SHIFT or LCD_R_L_LEFT_SHIFT 
/// @return bit code for shift 
extern unsigned char LCD_Create_Shift( unsigned char sc, unsigned char rl_shift )
{
    unsigned char code_shift = LCD_SHIFT;
    code_shift |= sc << LCD_S_C;
    code_shift |= rl_shift << LCD_R_L;
    return LCD_SHIFT_MASK & code_shift;
}

/// @brief Creates the bit code for function set
/// @param dl_bits LCD_DL_8_BITS or LCD_DL_4_BITS 
/// @param n_line LCD_N_TWO_LINES or LCD_N_ONE_LINE 
/// @param font LCD_F_FONT_1 or LCD_F_FONT_0  
/// @return bit code for function set 
extern unsigned char LCD_Create_Function_Set( unsigned char dl_bits, unsigned char n_line, unsigned char font )
{
    unsigned char function_set = LCD_FUNCTION_SET;
    function_set |= dl_bits << LCD_DL;
    function_set |= n_line << LCD_N;
    function_set |= font << LCD_F;
    return LCD_FUNCTION_SET_MASK & function_set;
}

/// @brief Creates the bit code for set cgram address 
/// @param address cgram address to set
/// @return bit code for cgram address 
extern unsigned char LCD_Create_Set_CGRAM_Address ( unsigned char cgram_address ) 
{
    return LCD_SET_CGRAM_ADDRESS_MASK & (cgram_address | LCD_SET_CGRAM_ADDRESS);
}

/// @brief Creates the bit code for ddram address 
/// @param address ddram address to set 
/// @return bit code for ddram address 
extern unsigned char LCD_Create_Set_DDRAM_Address ( unsigned char ddram_address )
{
    return LCD_SET_DDRAM_ADDRESS_MASK & (ddram_address | LCD_SET_DDRAM_ADDRESS);
}


//-----------------------------------------
// time functions   
//-----------------------------------------

/// @brief Function for waiting 
/// @param counter nano seconds 
extern void LCD_Delay(unsigned long counter)
{
	while(counter--);
}

/// @brief Waits, until the busy flag is not busy 
/// @param none 
extern void LCD_Wait_For_Busyflag( void ) 
{
    while (LCD_Read_Busyflag());
}





