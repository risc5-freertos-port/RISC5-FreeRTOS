//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

#include "spi.h"
#include "../Common_DE2_115.h"

//------------------------------------------------------------------------------
//         Local definitions
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//         Local variables
//------------------------------------------------------------------------------
static volatile unsigned long * status_register = (unsigned long *) SPI_STATUS_ADDRESS;
static volatile unsigned long * data_register = (unsigned long *) SPI_DATA_ADDRESS;


//------------------------------------------------------------------------------
//         Local functions
//------------------------------------------------------------------------------

/// @brief Initializes the SPI.
/// @param settings can have the following values which have to be connected by or:
/// SPI_NET_EN   -> enables the network
/// SPI_FAST     -> enables fast SPI
/// SPI_WIFI_SEL -> wifi chip select
/// SPI_SDC_SEL  -> sd card chip select
extern void SPI_Init( unsigned int settings )
{
    *status_register = SPI_SETTINGS_MASK & settings;
}

/// @brief Writes data to the selected slave
/// @param data is the data to be written
extern void SPI_Write( int data )
{
    *data_register = data;
}

/// @brief Reads data from selected slave
/// @param none 
/// @return data read from the slave
extern int SPI_Read( void )
{
    return *data_register;
}

/// @brief Checks if the SPI is ready 
/// @param none 
/// @return SPI_RDY if the SPI is ready
extern int SPI_Rdy( void )
{
   return *status_register & SPI_RDY;
}
