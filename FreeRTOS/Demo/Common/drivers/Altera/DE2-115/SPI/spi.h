#ifndef _SPI_H
#define _SPI_H

//------------------------------------------------------------------------------
//      Definitions
//------------------------------------------------------------------------------

#define SPI_DATA_ADDRESS          0xFFFFD0
#define SPI_STATUS_ADDRESS        0xFFFFD4

#define SPI_RDY     		        0x1
#define SPI_BUSY                    0x0
#define SPI_NET_EN                  ( 0x1 << 3 )
#define SPI_FAST                    ( 0x1 << 2 )
#define SPI_WIFI_SEL                ( 0x1 << 1 )
#define SPI_SDC_SEL                 0x1

#define SPI_SETTINGS_MASK           0xF

//------------------------------------------------------------------------------
//         Types
//------------------------------------------------------------------------------

typedef struct
{
    unsigned int net_en;
    unsigned int fast;
    unsigned int wifi_sel;
    unsigned int sdc_sel;
}Spi_Settings;

//------------------------------------------------------------------------------
//         Exported functions
//------------------------------------------------------------------------------

extern void SPI_Init( unsigned int settings );
extern void SPI_Write( int data );
extern int SPI_Read( void );
extern int SPI_Rdy( void );

#endif // _SPI_H

