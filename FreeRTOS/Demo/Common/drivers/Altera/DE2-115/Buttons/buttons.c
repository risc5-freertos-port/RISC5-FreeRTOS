//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

#include "buttons.h"

//------------------------------------------------------------------------------
//         Local definitions
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//         Local variables
//------------------------------------------------------------------------------

static unsigned long * data_register = (unsigned long *)  BTN_DATA_ADDRESS;

//------------------------------------------------------------------------------
//         Local functions
//------------------------------------------------------------------------------

/// @brief Returns the content of the data_register at address BTN_DATA_ADDRESS
/// @param  none 
/// @return The read data register with format: { press[3:0], release[3:0], 12'bx, button[3:0], switch[7:0] }
unsigned long Buttons_Read( void )
{   
    return  *data_register;
}

/// @brief Enables the button interrupt 
/// @param interrupt_flag The button interrupt which we want to be enabled
void Buttons_Set_Interrupt( unsigned long interrupt_flag )
{
    *data_register = interrupt_flag;
}

/// @brief Disables all button interrupts
/// @param none
void Buttons_Reset_Interrupt( void )
{
    *data_register = 0;
}
