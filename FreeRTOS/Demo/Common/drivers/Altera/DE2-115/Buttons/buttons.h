#ifndef _BUTTONS_H
#define _BUTTONS_H

//------------------------------------------------------------------------------
//      Definitions
//------------------------------------------------------------------------------

#define BTN_DATA_ADDRESS            0xFFFF90

#define BTN_STATE_0		            0x00000100 
#define BTN_STATE_1		            0x00000200 
#define BTN_STATE_2                 0x00000400 
#define BTN_STATE_3		            0x00000800 
#define BTN_STATE_ALL			    0x00000F00

#define BTN_PRESSED_0               0x10000000 
#define BTN_PRESSED_1               0x20000000 
#define BTN_PRESSED_2			    0x40000000 
#define BTN_PRESSED_3		        0x80000000 
#define BTN_PRESSED_ALL			    0xF0000000

#define BTN_RELEASED_0              0x01000000
#define BTN_RELEASED_1              0x02000000
#define BTN_RELEASED_2              0x04000000
#define BTN_RELEASED_3              0x08000000
#define BTN_RELEASED_ALL			0x0F000000

#define BTN_PRESSED_I_ENABLE_0      0x10000000
#define BTN_PRESSED_I_ENABLE_1      0x20000000
#define BTN_PRESSED_I_ENABLE_2      0x40000000
#define BTN_PRESSED_I_ENABLE_3      0x80000000
#define BTN_PRESSED_I_ENABLE_ALL    0xF0000000

#define BTN_RELEASED_I_ENABLE_0     0x01000000
#define BTN_RELEASED_I_ENABLE_1     0x02000000
#define BTN_RELEASED_I_ENABLE_2     0x04000000
#define BTN_RELEASED_I_ENABLE_3     0x08000000
#define BTN_RELEASED_I_ENABLE_ALL   0x0F000000

//------------------------------------------------------------------------------
//         Types
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//         Exported functions
//------------------------------------------------------------------------------

unsigned long Buttons_Read( void );
void Buttons_Set_Interrupt( unsigned long interrupt_flag );
void Buttons_Reset_Interrupt( void );

#endif // _BUTTONS_H
