#include "../../../../drivers/Altera/DE2-115/Buttons/buttons.h"
#include "../../../../drivers/Altera/DE2-115/LEDs/leds.h"

#define BTN_I_FLAG 0x8

void EnableGlobalInterrupts( void );
void EnableInterrupts( unsigned long );

unsigned int Button_State;

int edge_up = 0;
int edge_down = 0;
int led_state = 0;

unsigned char led_mask = 0;


void ISR_BTN(void)
{
    Button_State = Buttons_Read();
    // led_mask = Button_State >> 24;
    //led_mask = LED_01;
    if( (Button_State & BTN_PRESSED_1) == BTN_PRESSED_1 )
    {
        edge_up = 1;
        led_mask |= LED_02;
    }     
    if( (Button_State & BTN_RELEASED_1) == BTN_RELEASED_1 )
    {
        edge_down = 1;
        led_mask |= LED_03;
    }
    if( (edge_up == 1) && (edge_down == 1) )
    {
        //led_mask = led_mask & ~(LED_02 | LED_03);
        if(led_state == 0)
        {
            led_state = 1;
            led_mask |= LED_01;
        }
        else
        {
            led_state = 0;
            led_mask = led_mask & ~LED_01;
        }
        edge_up = 0;
        edge_down = 0;
    }
    LEDs_Write(led_mask);
}

int main( void )
{
    EnableGlobalInterrupts();
    EnableInterrupts( BTN_I_FLAG );
    Buttons_Set_Interrupt( BTN_RELEASED_I_ENABLE_1 | BTN_PRESSED_I_ENABLE_1 );
    LEDs_Write(0);
    while(1)
    {

    }
    return 0;
}
