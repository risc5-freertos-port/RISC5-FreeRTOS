#include "../../../../drivers/Altera/DE2-115/Switches/switches.h"
#include "../../../../drivers/Altera/DE2-115/LEDs/leds.h"


int main( void )
{
    unsigned long switch_vals_old = 0x00;
    while(1)
    {
        unsigned long switch_vals_new = Switches_Read();
        if(switch_vals_old != switch_vals_new)
        {
            LEDs_Write(switch_vals_new);
            switch_vals_old = switch_vals_new;
        }
    }
    return 0;
} 
