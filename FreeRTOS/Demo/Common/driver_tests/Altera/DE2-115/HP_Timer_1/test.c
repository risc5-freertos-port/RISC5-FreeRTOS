#include "../../../../drivers/Altera/DE2-115/HP_Timer/hp_timer.h"

#define HPTMR_1_I_FLAG 0x4000
#define TIMER_COUNT_LIM 10

extern void EnableGlobalInterrupts(void);
extern void EnableInterrupts(int);

unsigned int timer_counter = 0;

void ISR_HP_TIMER( void )
{
    unsigned long counter_val = 0;
    ++timer_counter;
    counter_val = HPTMR_1_Get_Counter();
    HPTMR_1_Reset_Interrupt_Flag();
}


int main( void )
{
    unsigned long counter_val;
    EnableGlobalInterrupts();
    EnableInterrupts(HPTMR_1_I_FLAG);
    HPTMR_1_Init(0xFFFF);
    HPTMR_1_Enable_Interrupt();
    
    while(timer_counter < TIMER_COUNT_LIM)
    {
        counter_val = HPTMR_1_Get_Counter();
    }
    HPTMR_1_Disable_Interrupt();
    
    return 0;
}
