#include "../../../../drivers/Altera/DE2-115/MS_Timer/ms_timer.h"
#include "../../../../drivers/Altera/DE2-115/LEDs/leds.h"

#define MS_TIMER_I_FLAG 0x0800
#define SECOND 500

void EnableGlobalInterrupts( void );
void EnableInterrupts( int );

unsigned long timer_val = 0;
int global_flag = 0;

void ISR_MS_TIMER( void )
{
    static int isr_counter = 0;
    
    if(isr_counter == SECOND)
    {
        if(global_flag == 1)
        {
            global_flag = 0;
        }
        else
        {
            global_flag = 1;
        }   
        isr_counter = 0;
    }
    ++isr_counter;
    timer_val = MS_Timer_Get_Counter();
}

int main(void)
{
    EnableGlobalInterrupts();
    EnableInterrupts( MS_TIMER_I_FLAG );
    MS_Timer_Enable_Interrupt();
    
    LEDs_Write( (unsigned int) ~LED_ALL );
    while(1)
    {
        if(global_flag == 0)
        {
            LEDs_Write(LED_03);
        }
        else
        {
            LEDs_Write(LED_04);
        }
    }

    return 0;
}

