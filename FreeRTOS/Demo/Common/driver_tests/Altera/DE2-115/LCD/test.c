#include "../../../../drivers/Altera/DE2-115/LCD/lcd.h"

// Test result if everything is right:
// |----------------|
// |LCDDI           | first line
// |AA              | second line
// |----------------|

int main(void)
{
    LCD_Init();

    // *** writes on first line *** 

    LCD_Write_Char('L');
    LCD_Write_Char('C');
    LCD_Write_Char('D');

    LCD_Set_Entry_Mode(LCD_I_D_DECREMENT, LCD_S_ACC_DISPLAY_SHIFT_NOT);

    if(LCD_Read_Data()) 
    {
        LCD_Set_Entry_Mode(LCD_I_D_INCREMENT, LCD_S_ACC_DISPLAY_SHIFT_NOT);
        LCD_Write_Char(LCD_Read_Data());
        LCD_Write_Char('I');
    }
    else 
    {
        LCD_Write_Char('E');
    }

    // *** writes on second line of the LCD ***
    LCD_Set_DDRAM_Address(LCD_SECOND_LINE_ADDR);
    LCD_Write_Char('A');
    LCD_Set_DDRAM_Address(LCD_SECOND_LINE_ADDR);
    LCD_Write_Char(LCD_Read_Data());
    
    return 0;
}
