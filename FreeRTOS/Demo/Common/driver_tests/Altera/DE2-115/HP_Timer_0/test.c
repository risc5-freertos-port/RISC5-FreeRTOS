#include "../../../../drivers/Altera/DE2-115/HP_Timer/hp_timer.h"

#define HPTMR_0_I_FLAG 0x8000
#define TIMER_COUNT_LIM 10

extern void EnableGlobalInterrupts(void);
extern void EnableInterrupts(int);

unsigned int timer_counter = 0;

void ISR_HP_TIMER( void )
{
    unsigned long counter_val = 0;
    ++timer_counter;
    counter_val = HPTMR_0_Get_Counter();
    HPTMR_0_Reset_Interrupt_Flag();
}


int main( void )
{
    unsigned long counter_val;
    EnableGlobalInterrupts();
    EnableInterrupts(HPTMR_0_I_FLAG);
    HPTMR_0_Init(0xFFFF);
    HPTMR_0_Enable_Interrupt();
    
    while(timer_counter < TIMER_COUNT_LIM)
    {
        counter_val = HPTMR_0_Get_Counter();
    }
    HPTMR_0_Disable_Interrupt();
    
    return 0;
}
