#include "../../../../drivers/Altera/DE2-115/Buttons/buttons.h"

#define BTN_I_FLAG 0x8

extern void EnableGlobalInterrupts(void);
extern void EnableInterrupts(int);

unsigned long Button_State;

void ISR_BTN(void)
{
    Button_State = Buttons_Read();
}

int main( void )
{
    EnableGlobalInterrupts();
    EnableInterrupts( BTN_I_FLAG );
    
    Buttons_Set_Interrupt( BTN_PRESSED_I_ENABLE_0 );
    while(1)
    {
        if( (Button_State & BTN_PRESSED_0) == BTN_PRESSED_0 )
        {
            break;
        }     
    }

    Buttons_Set_Interrupt( BTN_RELEASED_I_ENABLE_0 );
    while(1)
    {
        if( (Button_State & BTN_RELEASED_0) == BTN_RELEASED_0 )
        {
            break;
        }     
    }
        
    Buttons_Set_Interrupt( BTN_PRESSED_I_ENABLE_1 );
    while(1)
    {
        if( (Button_State & BTN_PRESSED_1) == BTN_PRESSED_1 )
        {
            break;
        }     
    }

    Buttons_Set_Interrupt( BTN_RELEASED_I_ENABLE_1 );
    while(1)
    {
        if( (Button_State & BTN_RELEASED_1) == BTN_RELEASED_1 )
        {
            break;
        }     
    }
     
    Buttons_Set_Interrupt( BTN_PRESSED_I_ENABLE_2 );
    while(1)
    {
        if( (Button_State & BTN_PRESSED_2) == BTN_PRESSED_2 )
        {
            break;
        }     
    }

    Buttons_Set_Interrupt( BTN_RELEASED_I_ENABLE_2 );
    while(1)
    {
        if( (Button_State & BTN_RELEASED_2) == BTN_RELEASED_2 )
        {
            break;
        }     
    }

    Buttons_Set_Interrupt( BTN_PRESSED_I_ENABLE_3 );
    while(1)
    {
        if( (Button_State & BTN_PRESSED_3) == BTN_PRESSED_3 )
        {
            break;
        }     
    }

    Buttons_Set_Interrupt( BTN_RELEASED_I_ENABLE_3 );
    while(1)
    {
        if( (Button_State & BTN_RELEASED_3) == BTN_RELEASED_3 )
        {
            break;
        }     
    }

    Buttons_Set_Interrupt( BTN_PRESSED_I_ENABLE_ALL );
    while(1)
    {
        if( (Button_State & BTN_PRESSED_ALL) == BTN_PRESSED_ALL )
        {
            break;
        }     
    }

    Buttons_Set_Interrupt( BTN_RELEASED_I_ENABLE_ALL );
    while(1)
    {
        if( (Button_State & BTN_RELEASED_ALL) == BTN_RELEASED_ALL )
        {
            break;
        }     
    }

    Buttons_Reset_Interrupt();
    return 0;
}
