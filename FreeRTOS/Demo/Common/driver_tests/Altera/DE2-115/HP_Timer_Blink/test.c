#include "../../../../drivers/Altera/DE2-115/HP_Timer/hp_timer.h"
#include "../../../../drivers/Altera/DE2-115/LEDs/leds.h"

#define HPTRM_0_I_FLAG  0x8000
#define HPTRM_1_I_FLAG  0x4000
#define HPTMR_O_CNT     0x2FAF080
#define HPTMR_1_CNT     0x17D7840

void EnableGlobalInterrupts( void );
void EnableInterrupts( int interrupt_flags );

unsigned int timer_counter = 0;
int led_state = 0;

void ISR_HPTMR_0( void )
{
    static int toggle;
    if(toggle == 1)
    {
        toggle = 0;
        led_state |= LED_01;
    }
    else
    {
        toggle = 1;
        led_state &= ~LED_01;
    }   
    HPTMR_0_Reset_Interrupt_Flag();
}

void ISR_HPTMR_1( void )
{
    static int toggle;
    if(toggle == 1)
    {
        toggle = 0;
        led_state |= LED_02;
    }
    else
    {
        toggle = 1;
        led_state &= ~LED_02;
    }   
    HPTMR_1_Reset_Interrupt_Flag();
}

int main( void )
{
    unsigned long counter_val;
    EnableGlobalInterrupts();
    EnableInterrupts(HPTRM_0_I_FLAG | HPTRM_1_I_FLAG);
    HPTMR_0_Enable_Interrupt();
    HPTMR_1_Enable_Interrupt();
    
    HPTMR_0_Init(HPTMR_O_CNT);
    HPTMR_1_Init(HPTMR_1_CNT);
    
    led_state = ~LED_ALL;
    while(1)
    {
        LEDs_Write(led_state);
    }
    
    return 0;
}
