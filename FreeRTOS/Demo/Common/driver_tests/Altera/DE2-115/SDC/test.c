#include "../../../../drivers/Altera/DE2-115/SPI/spi.h"
#include "../../../../drivers/Altera/DE2-115/SDC/sdc.h"
#include "../../../../drivers/Altera/DE2-115/RS232/rs232.h"

#define TESTED_BLOCKS   10
#define BLOCK           ( SDC_BLOCK_SIZE / SDC_WORD_SIZE )
#define BLOCK_BULK      ( BLOCK * 10 )
#define U_INT_DIGIT_LEN 10

void print_serial( char* string )
{
    while( *string != 0 )
    {
        RS232_Write_i( *string, RS232_0 );
        while( RS232_XMT_Ready( RS232_0 ) != RS232_XMT_RDY );
        string+=1;
    }
}

int main( void )
{
    unsigned int data_write[BLOCK];
    unsigned int data_read[BLOCK];
    unsigned int data_write_bulk[BLOCK_BULK];
    unsigned int data_read_bulk[BLOCK_BULK];
    int i;
    unsigned int block_addr;
    unsigned int sectors;
    char sectors_ascii[U_INT_DIGIT_LEN + 1];
    char ascii_char = 'A';
    int successful = 1;
    char * message = {"0000\t0000\t \n\r"};

    RS232_Init( RS232_BAUD_RATE_9600, RS232_0 );
    for( i=0; i<128; ++i )
    {
        data_write[i] = ascii_char;
        data_write[i] |= ascii_char << 8;
        data_write[i] |= ascii_char << 16;
        data_write[i] |= ascii_char << 24;
        if( ascii_char < 'Z' )
        {
            ++ascii_char;
        }
        else
        {
            ascii_char = 'A';
        }
    }

    ascii_char = '0';
    for( i=0; i<BLOCK_BULK; ++i )
    {
        data_write_bulk[i] = ascii_char;
        data_write_bulk[i] |= ascii_char << 8;
        data_write_bulk[i] |= ascii_char << 16;
        data_write_bulk[i] |= ascii_char << 24;
        if( ascii_char < '9' )
        {
            ++ascii_char;
        }
        else
        {
            ascii_char = '0';
        }
    }

    /* Repeated single block write/read */
    print_serial("Starting single block test:");
    for( block_addr = 0; block_addr < TESTED_BLOCKS ; ++block_addr)   
    {
        SDC_Write( block_addr, data_write, 1 );
        SDC_Read( block_addr, data_read, 1 );
        for( i=0; i<128; ++i )
        {
            message[0] = ( char ) data_write[i];
            message[1] = ( char ) ( data_write[i] >> 8 );
            message[2] = ( char ) ( data_write[i] >> 16 );
            message[3] = ( char ) ( data_write[i] >> 24 );

            message[5] = ( char ) data_read[i];
            message[6] = ( char ) ( data_read[i] >> 8 );
            message[7] = ( char ) ( data_read[i] >> 16 );
            message[8] = ( char ) ( data_read[i] >> 24 );
            if( data_write[i] != data_read[i] )
            {
                message[10] = 'X';
                successful = 0;
            }
            else
            {
                message[10] = ' ';
            }
            print_serial( message );
        }
    }

    /* Write/Read multiple blocks at once */
    print_serial("Starting multi block test:");
    SDC_Write( block_addr, data_write_bulk, 10 );
    SDC_Read( block_addr, data_read_bulk, 10 );
    for( i=0; i<BLOCK_BULK; ++i )
    {
        message[0] = ( char ) data_write_bulk[i];
        message[1] = ( char ) ( data_write_bulk[i] >> 8 );
        message[2] = ( char ) ( data_write_bulk[i] >> 16 );
        message[3] = ( char ) ( data_write_bulk[i] >> 24 );

        message[5] = ( char ) data_read_bulk[i];
        message[6] = ( char ) ( data_read_bulk[i] >> 8 );
        message[7] = ( char ) ( data_read_bulk[i] >> 16 );
        message[8] = ( char ) ( data_read_bulk[i] >> 24 );

        // message[2] = data_read_bulk[i];
        if( data_write_bulk[i] != data_read_bulk[i] )
        {
            message[10] = 'X';
            successful = 0;
        }
        else
        {
            message[10] = ' ';
        }
        print_serial( message );
    }

    if(!successful)
    {
        print_serial("NOT ");
    }
    print_serial("OK\n");
    print_serial("Sectors: ");

    sectors = SDC_Capacity();
    for( i = 0; i < U_INT_DIGIT_LEN; ++i)
    {
        sectors_ascii[i] = ' ';
    }
    sectors_ascii[U_INT_DIGIT_LEN] = 0;

    i = U_INT_DIGIT_LEN - 1;
    while ( sectors > 0 )
    {
        sectors_ascii[i] = '0' + ( sectors % 10 );
        sectors /= 10;
        --i;
    }
    print_serial(sectors_ascii);

    return 0;
}
