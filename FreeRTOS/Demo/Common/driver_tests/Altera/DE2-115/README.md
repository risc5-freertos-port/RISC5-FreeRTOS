# Button_Debounce_00
## Has user interaction: yes
## Description
Button 1 can be pressed and released to ensure debouncing.

# Expected output 
At start-up all LEDs should be off. After button 1 is pressed the first time LED 2 will light up indefinitely.
After button 1 is release LED 3 will turn on indefinitely.
LED 1 will toggel with each press and release cycle.

runs in simulator: yes 
runs in hardware: yes



# Buttons
## Has user interaction: yes
## Description 
Buttons need to be pressed an released in the following order:
- Button 0 press
- Button 0 release
- Button 1 press
- Button 1 release
- Button 2 press
- Button 2 release
- Button 3 press
- Button 3 release
- all Buttons press
- all Buttons release

To press and release the buttons the ss command can be used in the simulator.
Between states the simulator has to be continued with the c command.

## Expected output
Between steps progress can be seen through when unassembling the simulator.
After all buttons have been pressed, the program will stay in an endless loop.

runs in simulator: yes
runs in hardware: no


 
# HP_Timer_0
## Has user interaction: no 
## Description 
Checks if timer interrupt occurs.

## Expected output
Interrupt occurance can be checked by setting a breakpoint at address 4 (b 4).

runs in simulator: yes 
runs in hardware: yes but results cannot be seen


 
# HP_Timer_1
## Has user interaction: no 
## Description 
Checks if timer interrupt occurs.

## Expected output
Interrupt occurance can be checked by setting a breakpoint at address 4 (b 4).

runs in simulator: yes 
runs in hardware: yes but results cannot be seen



# HP_Timer_Blink
## Has user interaction: yes 
## Description 
Both HP-Timers are running and toggle LED 1 & 2 periodically.

## Expected output
LED 2 toggles twice as fast as LED 1.

runs in simulator: yes 
runs in hardware: yes but results cannot be seen


 
# LCD
## Has user interaction: No
## Description
This tests various functions of the lcd driver and gives an example how to use it. In this test both lcd lines are used.

## Expected output
Test result if everything is right:
 |----------------|
 |LCDDI           | first line
 |AA              | second line
 |----------------|

runs in simulator: yes
runs in hardware: yes


 
# LEDs
## Has user interaction: no
## Description
Tests the led writing. Can be used in simulator and at real hardware. In the simulator you can see the different states of the leds.
At real hardware you can just see the last result and no different stages. 

## Expected output
LED 3 and LED 5 are on

runs in simulator: yes
runs in hardware: yes but only the last step can be seen.


 
# MS_Timer
## Has user interaction: no 
## Description: 
The interrupt of the MS Timer is triggered 10 times.

## Expected output
In the simulator set a breakpoint at address 4 (b 4).
After that continue execution with the command c.
The programm should stop 10 times after continuing.

runs in simulator: yes
runs in hardware: no visible effect


 
# MS_Timer_Blink
## Has user interaction: yes 
## Description 
The MS-Timer toggles LED 3 & 4 in an alternating mode.

## Expected output
The MS-Timer toggles LED 3 & 4 in an alternating mode.

runs in simulator: yes 
runs in hardware: yes


 
# RS232
## Has user interaction: yes 
## Description
Test echos user input on the command prompt.
For use in simulator execute make run-connect on separate terminal after starting the test.
Key inputs can be done there. 

## Expected output
User input in terminal is visualised.

runs in simulator: yes 
runs in hardware: yes


 
# Switches_LEDs_00
## Has user interaction: yes
## Description
Tests the functionality of switches and leds. Each switch changes the state of the related led. Each switch has its own led.

## Expected output
The output depends on the switch states. If you change the state of one switch, the corresponding led state(on/off) should also. 

runs in simulator: yes
runs in hardware: yes


 
# SDC
## Has user interaction: no
## Description
Writes data to the SD-Card, reads it back and compares the results.
Also the storage capacity is measured. 

## Expected output
A table is generated as output. The first column shows the written data, the second column the read data.
If the data does not match an x is put in the third column to indicate a mistake.
If everything ran correctly the string 'OK' is displayed at the end, followed by the capacity in Bytes.
Should an error occure at any time the string 'NOT OK' is displayed at the end.

runs in simulator: yes
runs in hardware: yes
