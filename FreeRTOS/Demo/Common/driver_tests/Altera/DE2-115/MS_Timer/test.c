#include "../../../../drivers/Altera/DE2-115/MS_Timer/ms_timer.h"

#define MS_TIMER_I_FLAG 0x0800
#define TIMER_COUNT_LIM 10

extern void EnableGlobalInterrupts( void );
extern void EnableInterrupts( int );

unsigned long timer_counter = 0;
unsigned long timer_val = 0;

void ISR_MS_TIMER( void )
{
    timer_val = MS_Timer_Get_Counter();
    ++timer_counter;
}


int main(void)
{
    EnableGlobalInterrupts();
    EnableInterrupts( MS_TIMER_I_FLAG );
    MS_Timer_Enable_Interrupt();
    
    while(timer_counter < (unsigned char) TIMER_COUNT_LIM);
    MS_Timer_Disable_Interrupt();

    return 0;
}

