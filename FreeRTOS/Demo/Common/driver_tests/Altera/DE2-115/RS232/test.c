#include "../../../../drivers/Altera/DE2-115/RS232/rs232.h"
#include "../../../../../Common/drivers/Altera/DE2-115/Common_DE2_115.h"

#define RS232_RCV_I_FLAG 0x0080
#define RS232_XMT_I_FLAG 0x0040

// To execute this test first execute make run and then in a 
// seperate window make run-connect. 
// On the make run-connect window you should see an echo of all chars 
// you input with your keyboard. 

void EnableGlobalInterrupts( void );
void EnableInterrupts( unsigned int );
unsigned char c;
    
void ISR_RS232_RCV(void)
{
    c = RS232_Read_i(RS232_0);
    RS232_XMT_Enable_Interrupt(RS232_0);
}

void ISR_RS232_XMT(void)
{
    RS232_XMT_Disable_Interrupt(RS232_0);
    RS232_Write_i(c, RS232_0);
}

int main(void)
{
    EnableGlobalInterrupts();
    EnableInterrupts(RS232_RCV_I_FLAG | RS232_XMT_I_FLAG);

    RS232_Init(RS232_BAUD_RATE_9600, RS232_0);
    RS232_RCV_Enable_Interrupt(RS232_0);
    while(1);
    return 0;
}
