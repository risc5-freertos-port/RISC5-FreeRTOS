 /*
 * "Button and LCD test" - This creates two tasks.  The first simply scrolls
 * a message back and forth along the top line of the LCD display.  If no
 * buttons are pushed, the second also scrolls a message back and forth, but
 * along the bottom line of the display.  The automatic scrolling of the second
 * line of the display can be started and stopped using button SW2.  Once 
 * stopped it can then be manually nudged left using button SW3, and manually
 * nudged right using button SW1.  Button pushes generate an interrupt, and the
 * interrupt communicates with the task using a queue.
*/

/*
 * FreeRTOS V202112.00
 * Copyright (C) 2020 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * https://www.FreeRTOS.org
 * https://aws.amazon.com/freertos
 *
 */

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include <risc5.h>
#include <string.h>

/* Hardware specifics. */
#include "buttons.h"
#include "lcd.h"

/* LCD line positions */
#define lcdLINE1 			0
#define lcdLINE2			16

/* States used by the LCD tasks. */
#define	lcdRIGHT_TO_LEFT	0U
#define	lcdLEFT_TO_RIGHT	1U
#define	lcdRUNNING			0U

/* Characters on each line. */
#define lcdSTRING_LEN		16 

/* Commands sent from the IRQ to the task controlling the second line of the
display. */
#define lcdSHIFT_BACK_COMMAND		0x01U
#define lcdSTART_STOP_COMMAND		0x02U
#define lcdSHIFT_FORWARD_COMMAND	0x03U

/* The length of the queue used to send commands from the ISRs. */
#define lcdCOMMAND_QUEUE_LENGTH		32U

/* Defines the minimum time that must pass between consecutive button presses
to accept a button press as a unique press rather than just a bounce. */
#define lcdMIN_TIME_BETWEEN_INTERRUPTS_MS ( 125UL / portTICK_PERIOD_MS )

#define lcdSPEED1 300
#define lcdSPEED2 200

/* 
 * Setup the IO needed for the buttons to generate interrupts. 
 */
static void prvSetupButtonIOAndInterrupts( void );

/*
 * A task that simply scrolls a string from left to right, then back from the
 * right to the left.  This is done on the first line of the display.
 */
static void prvLCDTaskLine1( void *pvParameters );

/*
 * If no buttons are pushed, then this task acts as per prvLCDTaskLine1(), but
 * using the second line of the display.
 *
 * Using the buttons, it is possible to start and stop the scrolling of the 
 * text.  Once the scrolling has been stopped, other buttons can be used to
 * manually scroll the text either left or right.
 */ 
static void prvLCDTaskLine2( void *pvParameters );

/*
 * Looks at the direction the string is currently being scrolled in, and moves
 * the index into the portion of the string that can be seen on the display
 * either forward or backward as appropriate. 
 */
static void prvScrollString( unsigned char *pucDirection, short *pusPosition, size_t xStringLength );

/* 
 * Displays lcdSTRING_LEN characters starting from pcString on the line of the
 * display requested by ucLine.
 */
static void prvDisplayNextString( unsigned char ucLine, char *pcString );

/*
 * Called from the IRQ interrupts, which are generated on button pushes.  Send
 * ucCommand to the task on the button command queue if 
 * lcdMIN_TIME_BETWEEN_INTERRUPTS_MS milliseconds have passed since the button
 * was last pushed (for debouncing). 
 */
static portBASE_TYPE prvSendCommandOnDebouncedInput( TickType_t *pxTimeLastInterrupt, unsigned char ucCommand );

/*-----------------------------------------------------------*/

/* The queue used to pass commands from the button interrupt handlers to the
prvLCDTaskLine2() task. */
static QueueHandle_t xButtonCommandQueue = NULL;

/* The mutex used to ensure only one task writes to the display at any one
time. */
static SemaphoreHandle_t xLCDMutex = NULL;

/* The string that is scrolled up and down the first line of the display. */
static const char cDataString1[] = "        https://www.FreeRTOS.org        ";

/* The string that is scrolled/nudged up and down the second line of the 
display. */
static const char cDataString2[] = "        RISC5 Port on Altera DE2-115        ";

/* Structures passed into the two tasks to inform them which line to use on the
display, how long to delay for, and which string to use. */
struct _LCD_Params 
{
	int Line;
	int Speed;
	char * ptr_str; 
};

struct _LCD_Params xLCDLine1 = 
{
	lcdLINE1, 1000, ( char * ) cDataString1	
};

struct _LCD_Params xLCDLine2 = 
{
	lcdLINE2, 1000, ( char * ) cDataString2
};
/*-----------------------------------------------------------*/

static void prvBUTTON_IRQ( void )
{
static unsigned char ucCommand;
portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
unsigned long button_state = Buttons_Read();

	if( button_state & BTN_PRESSED_1 )
	{
		ucCommand = lcdSHIFT_BACK_COMMAND;
	}
	else if ( button_state & BTN_PRESSED_2 )
	{
		ucCommand = lcdSTART_STOP_COMMAND;
	}
	else if ( button_state & BTN_PRESSED_3 )
	{
		ucCommand = lcdSHIFT_FORWARD_COMMAND;
	}
	else
	{
		return;
	}

	xQueueSendToBackFromISR( xButtonCommandQueue, &ucCommand, &xHigherPriorityTaskWoken );
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
/*-----------------------------------------------------------*/

void vStartButtonAndLCDDemo( void )
{
	Buttons_Set_Interrupt( BTN_PRESSED_I_ENABLE_1 | BTN_PRESSED_I_ENABLE_2 | BTN_PRESSED_I_ENABLE_3 );
	LCD_Init();
	risc5_ivt[RISC5_IRQ_BTNSWT] = prvBUTTON_IRQ;

	/* Create the mutex used to guard the LCD. */
	xLCDMutex = xSemaphoreCreateMutex();
	configASSERT( xLCDMutex );
	
	/* Create the queue used to pass commands from the IRQ interrupts to the
	prvLCDTaskLine2() task. */
	xButtonCommandQueue = xQueueCreate( lcdCOMMAND_QUEUE_LENGTH, sizeof( unsigned char ) );
	configASSERT( xButtonCommandQueue );

	/* Start the two tasks as described at the top of this file. */
	xTaskCreate( prvLCDTaskLine1, "LCD1", configMINIMAL_STACK_SIZE * 3, ( void * ) &xLCDLine1, tskIDLE_PRIORITY + 1, NULL );
	xTaskCreate( prvLCDTaskLine2, "LCD2", configMINIMAL_STACK_SIZE * 3, ( void * ) &xLCDLine2, tskIDLE_PRIORITY + 2, NULL );
}
/*-----------------------------------------------------------*/

static void prvScrollString( unsigned char *pucDirection, short *pusPosition, size_t xStringLength )
{
	/* Check which way to scroll. */
	if( *pucDirection == lcdRIGHT_TO_LEFT )
	{
		/* Move to the next character. */
		( *pusPosition )++;
		
		/* Has the end of the string been reached? */
		if( ( *pusPosition ) >= ( xStringLength - ( lcdSTRING_LEN - 1 ) ) )
		{
			*pusPosition = xStringLength - ( lcdSTRING_LEN - 1 );
			/* Switch direction. */
			*pucDirection = lcdLEFT_TO_RIGHT;
			( *pusPosition )--;				
		}
	}
	else
	{
		/* Move (backward) to the next character. */
		( *pusPosition )--;
		if( *pusPosition <= 0 )
		{
			*pusPosition = 0;
			/* Switch Direction. */
			*pucDirection = lcdRIGHT_TO_LEFT;				
		}
	}
}
/*-----------------------------------------------------------*/

static void prvDisplayNextString( unsigned char ucLine, char *pcString )
{
static char cSingleLine[ lcdSTRING_LEN + 1 ];

	xSemaphoreTake( xLCDMutex, portMAX_DELAY );
	memcpy( cSingleLine, pcString, lcdSTRING_LEN );
	LCD_Write_String( ucLine, cSingleLine, lcdSTRING_LEN );
	xSemaphoreGive( xLCDMutex );
}
/*-----------------------------------------------------------*/

static void prvLCDTaskLine1( void *pvParameters )
{
struct _LCD_Params *pxLCDParamaters = ( struct _LCD_Params * ) pvParameters;
short usPosition = 0;
unsigned char ucDirection = lcdRIGHT_TO_LEFT;
	
	while( 1 )
	{
		vTaskDelay( pxLCDParamaters->Speed / portTICK_PERIOD_MS );		

		/* Write the string. */
		prvDisplayNextString( pxLCDParamaters->Line, &( pxLCDParamaters->ptr_str[ usPosition ] ) );

		/* Move the string in whichever direction the scroll is currently going
		in. */
		prvScrollString( &ucDirection, &usPosition, strlen( pxLCDParamaters->ptr_str ) );
	}
}
/*-----------------------------------------------------------*/

static void prvLCDTaskLine2( void *pvParameters )
{
struct _LCD_Params *pxLCDParamaters = ( struct _LCD_Params * ) pvParameters;
short usPosition = 0;
unsigned char ucDirection = lcdRIGHT_TO_LEFT, ucStatus = lcdRUNNING, ucQueueData;
TickType_t xDelayTicks = 200;
	
	while( 1 )
	{
		/* Wait for a message from an IRQ handler. */
		if( xQueueReceive( xButtonCommandQueue, &ucQueueData, xDelayTicks ) != pdPASS )
		{
			/* A message was not received before xDelayTicks ticks passed, so
			generate the next string to display and display it. */
			prvDisplayNextString( pxLCDParamaters->Line, &( pxLCDParamaters->ptr_str[ usPosition ] ) );
			
			/* Move the string in whichever direction the scroll is currently 
			going in. */
			prvScrollString( &ucDirection, &usPosition, strlen( pxLCDParamaters->ptr_str ) );			
		}
		else
		{
			/* A command was received.  Process it. */
			switch( ucQueueData )
			{
				case lcdSTART_STOP_COMMAND :

					/* If the LCD is running, stop it.  If the LCD is stopped, start
					it. */
					ucStatus = !ucStatus;
					
					if( ucStatus == lcdRUNNING )
					{
						xDelayTicks = 200;
					}
					else
					{
						xDelayTicks = portMAX_DELAY;
					}
					break;

					
				case lcdSHIFT_BACK_COMMAND :

					if( ucStatus != lcdRUNNING )
					{
						/* If not already at the start of the display.... */
						if( usPosition > 0 )
						{
							/* ....move the display position back by one char. */
							usPosition--;												
							prvDisplayNextString( pxLCDParamaters->Line, &( pxLCDParamaters->ptr_str[ usPosition ] ) );
						}
					}
					break;
				
				
				case lcdSHIFT_FORWARD_COMMAND :

					if( ucStatus != lcdRUNNING )
					{
						/* If not already at the end of the display.... */
						if( usPosition < ( strlen( pxLCDParamaters->ptr_str ) - ( lcdSTRING_LEN ) ) )
						{
							/* ....move the display position forward by one 
							char. */
							usPosition++;
							prvDisplayNextString( pxLCDParamaters->Line, &( pxLCDParamaters->ptr_str[ usPosition ] ) );
						}
					}
					break;
			}
		}
	}
}
/*-----------------------------------------------------------*/
