#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

/* Here is a good place to include header files that are required across
your application. */

#define configMAX_API_CALL_INTERRUPT_PRIORITY   15  /* priorities higher than this one cannot be interrupted or blocked by FreeRTOS */
#define configKERNEL_INTERRUPT_PRIORITY         11  /* defining tick timer priority */

#define configUSE_PREEMPTION                    1
#define configUSE_PORT_OPTIMISED_TASK_SELECTION 0
#define configUSE_TICKLESS_IDLE                 0
#define configCPU_CLOCK_HZ                      50000000
#define configTICK_RATE_HZ                      1000
#define configMAX_PRIORITIES                    5
#define configMINIMAL_STACK_SIZE                1024 * 10
#define configMAX_TASK_NAME_LEN                 16
#define configUSE_16_BIT_TICKS                  0
#define configIDLE_SHOULD_YIELD                 1
#define configUSE_TASK_NOTIFICATIONS            1
#define configTASK_NOTIFICATION_ARRAY_ENTRIES   3
#define configUSE_MUTEXES                       1
#define configUSE_RECURSIVE_MUTEXES             1
#define configUSE_COUNTING_SEMAPHORES           1
#define configUSE_ALTERNATIVE_API               0 /* Deprecated! */
#define configQUEUE_REGISTRY_SIZE               10
#define configUSE_QUEUE_SETS                    1
#define configUSE_TIME_SLICING                  1       // because the RegTest tasks have same priority
#define configUSE_NEWLIB_REENTRANT              0
#define configENABLE_BACKWARD_COMPATIBILITY     0
#define configNUM_THREAD_LOCAL_STORAGE_POINTERS 5
#define configUSE_MINI_LIST_ITEM                1
#define configSTACK_DEPTH_TYPE                  uint16_t
#define configMESSAGE_BUFFER_LENGTH_TYPE        size_t
#define configHEAP_CLEAR_MEMORY_ON_FREE         1

/* Memory allocation related definitions. */
#define configSUPPORT_STATIC_ALLOCATION             0
#define configSUPPORT_DYNAMIC_ALLOCATION            1
#define configTOTAL_HEAP_SIZE                       524288 * 10
#define configAPPLICATION_ALLOCATED_HEAP            0
#define configSTACK_ALLOCATION_FROM_SEPARATE_HEAP   0

/* Hook function related definitions. */
#define configUSE_IDLE_HOOK                     0
#define configUSE_TICK_HOOK                     0
#define configCHECK_FOR_STACK_OVERFLOW          0
#define configUSE_MALLOC_FAILED_HOOK            0
#define configUSE_DAEMON_TASK_STARTUP_HOOK      0
#define configUSE_SB_COMPLETED_CALLBACK         0

/* Run time and task stats gathering related definitions. */
#define configGENERATE_RUN_TIME_STATS           0
#define configUSE_TRACE_FACILITY                0
#define configUSE_STATS_FORMATTING_FUNCTIONS    0

/* Software timer related definitions. */
#define configUSE_TIMERS                        1
#define configTIMER_TASK_PRIORITY               3
#define configTIMER_QUEUE_LENGTH                10
#define configTIMER_TASK_STACK_DEPTH            configMINIMAL_STACK_SIZE

#define INCLUDE_vTaskDelay                      1
#define INCLUDE_eTaskGetState                   1
#define INCLUDE_vTaskSuspend                    1
#define INCLUDE_xTaskDelayUntil                 1
#define INCLUDE_uxTaskPriorityGet               1
#define INCLUDE_xTimerPendFunctionCall          1
#define INCLUDE_vTaskPrioritySet                1
#define INCLUDE_vTaskDelete                     1
#define INCLUDE_xTaskAbortDelay                 1
#define INCLUDE_xTaskGetHandle                  1
#define INCLUDE_xSemaphoreGetMutexHolder        1

#define configPRINTF( X ) printf X

#define configSTART_TASK_NOTIFY_TESTS             0     
#define configSTART_TASK_NOTIFY_ARRAY_TESTS       0     
#define configSTART_BLOCKING_QUEUE_TESTS          0     
#define configSTART_SEMAPHORE_TESTS               0     
#define configSTART_POLLED_QUEUE_TESTS            0     
#define configSTART_INTEGER_MATH_TESTS            0    
#define configSTART_GENERIC_QUEUE_TESTS           0     
#define configSTART_PEEK_QUEUE_TESTS              0     
#define configSTART_MATH_TESTS                    0     
#define configSTART_RECURSIVE_MUTEX_TESTS         0     
#define configSTART_COUNTING_SEMAPHORE_TESTS      0     
#define configSTART_QUEUE_SET_TESTS               0     
#define configSTART_QUEUE_OVERWRITE_TESTS         0     
#define configSTART_EVENT_GROUP_TESTS             0     
#define configSTART_INTERRUPT_SEMAPHORE_TESTS     0     
#define configSTART_QUEUE_SET_POLLING_TESTS       0     
#define configSTART_BLOCK_TIME_TESTS              0     
#define configSTART_ABORT_DELAY_TESTS             0     
#define configSTART_MESSAGE_BUFFER_TESTS          0     
#define configSTART_STREAM_BUFFER_TESTS           0     
#define configSTART_STREAM_BUFFER_INTERRUPT_TESTS 0     
#define configSTART_TIMER_TESTS                   0     
#define configSTART_INTERRUPT_QUEUE_TESTS         0     
#define configSTART_REGISTER_TESTS                0     
#define configSTART_DELETE_SELF_TESTS             0     

/* Define to trap errors during development. */
void vAssertCalled( const char *fileName, int line );
#define configASSERT(expr) if (expr) {} else vAssertCalled(__FILE__, __LINE__)

#endif /* FREERTOS_CONFIG_H */
