#include "FreeRTOS.h"
#include "task.h"

#include "TestRunner.h"

#include "risc5.h"
#include "stdio.h"

void main( void )
{
    /* Startup and Hardware initialization. */

    /* Start tests. */
    vStartTests();

    /* Should never reach here. */
    while(0);
}

void vAssertCalled( const char *fileName, int line )
{
    volatile unsigned int resume = 0;

	( void ) fileName;
	( void ) line;


#ifdef CI_MODE
	*RISC5_IO_SIM_SHUTDOWN = (unsigned char)-1;
#endif

	taskENTER_CRITICAL();
	{
		printf("%s:%d: Assertion failed\n", fileName, line);

		/* Set resume to a non-zero value using the debugger to step out of this
		function. */
		while( !resume )
		{
			portNOP();
		}
	}
	taskEXIT_CRITICAL();
}


void putchar(char c) {
  if (c == '\n') {
    putchar('\r');
  }
  while ((*RISC5_IO_RS232_0_STAT_CTRL & 2) == 0) ;
  *RISC5_IO_RS232_0_DATA = c;
}
