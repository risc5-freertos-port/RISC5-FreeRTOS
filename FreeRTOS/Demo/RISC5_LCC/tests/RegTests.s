// imports
.GLOBAL ulRegisterTest1Counter
.GLOBAL ulRegisterTest2Counter
.GLOBAL ulRegisterTest3Counter
.GLOBAL ulRegisterTest4Counter
.GLOBAL vPortYield

// exports
.GLOBAL prvRegisterTest1Task
.GLOBAL prvRegisterTest2Task
.GLOBAL prvRegisterTest3Task
.GLOBAL prvRegisterTest4Task

.CODE

prvRegisterTest1Task:
    MOV R2, 0x02
    MOV R3, 0x03
    MOV R4, 0x04
    MOV R5, 0x05
    MOV R6, 0x06
    MOV R7, 0x07
    MOV R8, 0x08
    MOV R9, 0x09
    MOV R10, 0x0A
    MOV R11, 0x0B
    MOV R15, 0x0F

loop1:
    SUB R0, R2, 0x02
    BNE error_loop
    SUB R0, R3, 0x03
    BNE error_loop
    SUB R0, R4, 0x04
    BNE error_loop
    SUB R0, R5, 0x05
    BNE error_loop
    SUB R0, R6, 0x06
    BNE error_loop
    SUB R0, R7, 0x07
    BNE error_loop
    SUB R0, R8, 0x08
    BNE error_loop
    SUB R0, R9, 0x09
    BNE error_loop
    SUB R0, R10, 0x0A
    BNE error_loop
    SUB R0, R11, 0x0B
    BNE error_loop
    SUB R0, R15, 0x0F
    BNE error_loop

    MOV R0, ulRegisterTest1Counter
    LDW R1, R0, 0
    ADD R1, R1, 1
    STW R1, R0, 0

    B loop1
 

prvRegisterTest2Task:
    MOV R0, 0x1C
    MOV R1, 0x11
    MOV R4, 0x14
    MOV R5, 0x15
    MOV R6, 0x16
    MOV R7, 0x17
    MOV R8, 0x18
    MOV R9, 0x19
    MOV R10, 0x1A
    MOV R11, 0x1B
    MOV R15, 0x1F

loop2:
    SUB R2, R0, 0x1C
    BNE error_loop
    SUB R2, R1, 0x11
    BNE error_loop
    SUB R2, R4, 0x14
    BNE error_loop
    SUB R2, R5, 0x15
    BNE error_loop
    SUB R2, R6, 0x16
    BNE error_loop
    SUB R2, R7, 0x17
    BNE error_loop
    SUB R2, R8, 0x18
    BNE error_loop
    SUB R2, R9, 0x19
    BNE error_loop
    SUB R2, R10, 0x1A
    BNE error_loop
    SUB R2, R11, 0x1B
    BNE error_loop
    SUB R2, R15, 0x1F
    BNE error_loop

    MOV R2, ulRegisterTest2Counter
    LDW R3, R2, 0
    ADD R3, R3, 1
    STW R3, R2, 0

    B loop2

prvRegisterTest3Task:
    MOV R8, 0x38
    MOV R9, 0x39
    MOV R10, 0x3A
    MOV R11, 0x3B

loop3:
    C vPortYield

    SUB R0, R8, 0x38
    BNE error_loop
    SUB R0, R9, 0x39
    BNE error_loop
    SUB R0, R10, 0x3A
    BNE error_loop
    SUB R0, R11, 0x3B
    BNE error_loop

    MOV R0, ulRegisterTest3Counter
    LDW R1, R0, 0
    ADD R1, R1, 1
    STW R1, R0, 0

    B loop3

prvRegisterTest4Task:
    MOV R8, 0x48
    MOV R9, 0x49
    MOV R10, 0x4A
    MOV R11, 0x4B

loop4:
    C vPortYield

    SUB R0, R8, 0x48
    BNE error_loop
    SUB R0, R9, 0x49
    BNE error_loop
    SUB R0, R10, 0x4A
    BNE error_loop
    SUB R0, R11, 0x4B
    BNE error_loop

    MOV R0, ulRegisterTest4Counter
    LDW R1, R0, 0
    ADD R1, R1, 1
    STW R1, R0, 0

    B loop4

error_loop:
    B error_loop
    